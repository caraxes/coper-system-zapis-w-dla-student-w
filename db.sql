-- MySQL dump 10.13  Distrib 5.5.32, for Linux (x86_64)
--
-- Host: db.hostpark.pl    Database: webcrxpl_coper
-- ------------------------------------------------------
-- Server version	5.5.30-MariaDB-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_dziekanat` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,'','',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'1',':desc',1,'2011-12-18 16:25:27','0000-00-00 00:00:00'),(3,'1','opis\r\n',1,'2011-12-18 16:28:22','0000-00-00 00:00:00'),(4,'1','sasas',1,'2011-12-18 16:30:55','0000-00-00 00:00:00'),(5,'1','sasaass',1,'2011-12-18 16:31:06','0000-00-00 00:00:00'),(6,'1','Rozklad zajec',1,'2012-01-08 16:06:17','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daty_zapisow`
--

DROP TABLE IF EXISTS `daty_zapisow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daty_zapisow` (
  `od` datetime NOT NULL,
  `do` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daty_zapisow`
--

LOCK TABLES `daty_zapisow` WRITE;
/*!40000 ALTER TABLE `daty_zapisow` DISABLE KEYS */;
INSERT INTO `daty_zapisow` VALUES ('2012-01-08 11:00:00','2014-08-08 16:28:00');
/*!40000 ALTER TABLE `daty_zapisow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kursy`
--

DROP TABLE IF EXISTS `kursy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kursy` (
  `id_kurs` int(11) NOT NULL AUTO_INCREMENT,
  `id_prowadzacy` int(11) NOT NULL,
  `nazwa` varchar(200) NOT NULL,
  `opis` text NOT NULL,
  `ects` int(11) NOT NULL,
  `forma_zaliczenia` int(11) NOT NULL COMMENT '1-egzamin, 2-zaliczenie',
  `kod_kursu` varchar(110) NOT NULL,
  `kod_grupy` varchar(110) NOT NULL,
  `termin` text NOT NULL,
  `miejsca` int(11) NOT NULL,
  `miejsca_minimum` int(11) NOT NULL,
  `platny` int(11) NOT NULL DEFAULT '0' COMMENT '1-tak, 0- nie',
  `ile_platny` int(11) NOT NULL DEFAULT '100' COMMENT 'ile platny',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'status produktu',
  `date_add` datetime NOT NULL,
  `login_add` varchar(55) NOT NULL,
  `date_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login_mod` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id_kurs`),
  KEY `fk_category` (`nazwa`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='produkty';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kursy`
--

LOCK TABLES `kursy` WRITE;
/*!40000 ALTER TABLE `kursy` DISABLE KEYS */;
INSERT INTO `kursy` VALUES (1,2,'Rachunkowość dla inżyniera','opis Rachunkowość dla inżyniera',6,1,'EKZ000328W','Z05-44a','PN 18:55-20:35 B-4 (409 )',2,1,0,0,1,'2011-12-16 20:39:04','dodawanie','2011-12-16 19:39:04',NULL),(2,2,'Analiza Matematyczna 1a1111','opis analizaaaaaaaaaa',1,2,'EKZ009501Cccc','Z05-44accccc','aaaaa?R 17:05-20:35 C-7 (1014 )',4,2,1,400,1,'2011-12-16 20:43:13','dodawanie','2011-12-16 19:43:13',NULL),(3,2,'sdsadfsaf','fdsfsdf',1,1,'fsdfdsfds','fdsfdsfsd','fdsfdfsd',14,6,0,0,1,'2012-01-03 00:53:15','dodawanie','2012-01-02 23:53:15',NULL),(4,11,'Technologie Informacyjne','laboratorium',2,2,'IZ4567','a01','9:00-11:00',5,3,0,0,1,'2012-01-08 16:08:41','dodawanie','2012-01-08 15:08:41',NULL);
/*!40000 ALTER TABLE `kursy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oplaty`
--

DROP TABLE IF EXISTS `oplaty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oplaty` (
  `id_oplata` int(11) NOT NULL AUTO_INCREMENT,
  `id_kurs` int(11) NOT NULL,
  `id_student` int(11) NOT NULL COMMENT 'id usera ze statusem student',
  `id_dziekanat` int(11) NOT NULL COMMENT 'id usera ze statusem dziekanat',
  `id_status_oplat` int(11) NOT NULL,
  `data_dodania` datetime NOT NULL,
  `data_operacji_koncowej` datetime NOT NULL,
  PRIMARY KEY (`id_oplata`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oplaty`
--

LOCK TABLES `oplaty` WRITE;
/*!40000 ALTER TABLE `oplaty` DISABLE KEYS */;
INSERT INTO `oplaty` VALUES (6,2,3,1,1,'2011-12-18 13:07:06','0000-00-00 00:00:00'),(7,2,4,1,2,'2011-12-18 13:57:15','0000-00-00 00:00:00'),(8,2,15,1,1,'2012-01-08 16:25:51','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `oplaty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specjalnosc`
--

DROP TABLE IF EXISTS `specjalnosc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specjalnosc` (
  `id_specjalnosc` int(11) NOT NULL AUTO_INCREMENT,
  `id_wydzial` int(11) NOT NULL,
  `ilosc_sem` int(11) NOT NULL,
  `nazwa` varchar(300) COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_specjalnosc`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specjalnosc`
--

LOCK TABLES `specjalnosc` WRITE;
/*!40000 ALTER TABLE `specjalnosc` DISABLE KEYS */;
INSERT INTO `specjalnosc` VALUES (1,1,7,'Inżynier Informatyka','Studia inz bla bla bla'),(2,1,3,'Magister Informatyka','Studia magisterskie'),(3,2,7,'Inzynier Zarzadzanie','iznynier zarzadzanie'),(4,2,3,'Magister Zarzadzanie','Magister Zarzadzanie');
/*!40000 ALTER TABLE `specjalnosc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_oplat`
--

DROP TABLE IF EXISTS `status_oplat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_oplat` (
  `id_status_oplat` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(300) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_status_oplat`),
  UNIQUE KEY `id_status_oplat` (`id_status_oplat`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_oplat`
--

LOCK TABLES `status_oplat` WRITE;
/*!40000 ALTER TABLE `status_oplat` DISABLE KEYS */;
INSERT INTO `status_oplat` VALUES (1,'Oplacone - OK'),(2,'Odrzucone');
/*!40000 ALTER TABLE `status_oplat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stopien`
--

DROP TABLE IF EXISTS `stopien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stopien` (
  `id_stopien` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_stopien`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stopien`
--

LOCK TABLES `stopien` WRITE;
/*!40000 ALTER TABLE `stopien` DISABLE KEYS */;
INSERT INTO `stopien` VALUES (1,'doktor'),(2,'doktor habilitowany'),(3,'profesor');
/*!40000 ALTER TABLE `stopien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `name` varchar(85) NOT NULL COMMENT 'imie',
  `surname` varchar(85) NOT NULL COMMENT 'nazwisko',
  `pesel` varchar(11) DEFAULT NULL,
  `address` text NOT NULL COMMENT 'nazwa ulicy, numer domu, numer lokalu',
  `address_zip_number` varchar(6) NOT NULL COMMENT 'kod pocztowy',
  `address_city_name` varchar(145) NOT NULL COMMENT 'miejscowosc',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT 'login jest adresem e-mail',
  `password` varchar(55) DEFAULT NULL COMMENT 'hasło',
  `mobile_number` varchar(45) DEFAULT NULL COMMENT 'dodatkowy telefon kontaktowy',
  `bank_name` varchar(245) DEFAULT NULL COMMENT 'nazwa banku',
  `bank_account` varchar(26) DEFAULT NULL COMMENT 'numer konta',
  `index` int(11) NOT NULL,
  `nr_pracownika` varchar(100) NOT NULL,
  `dataSTART` datetime NOT NULL,
  `dataKONIEC` datetime NOT NULL,
  `id_specjalnosc` int(11) NOT NULL,
  `id_stopien` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'status konta w systemie',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'typ osoby, 1 dziekanat, 2 wykladowca, 3 student',
  `date_add` datetime NOT NULL,
  `login_add` varchar(55) DEFAULT NULL,
  `date_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login_mod` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id_user`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Klienci';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Pracownik Dziekanatu','Jola Stara','87562347474','ul. Wiaskowa 212/33','11-212','WROCLAW','jola.stara@email.com','cc03e747a6afbbcbf8be7668acfebee5','654786125','alianz','628462348924896482548',0,'1234','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,1,1,'0000-00-00 00:00:00',NULL,'2011-12-15 19:40:00',NULL),(2,'Jacek','Placek','3629263263','ul. Powiazana 13','11-222','POZNAN','jacek.placek@email.com','cc03e747a6afbbcbf8be7668acfebee5','98769876585','mbank','4623482364396236364',0,'XXX006','0000-00-00 00:00:00','0000-00-00 00:00:00',1,3,1,2,'0000-00-00 00:00:00',NULL,'2011-12-15 19:44:13',NULL),(3,'Lukasz','Jakis','63293623923','ul. Wiezienna 23','23-222','WROCLAW','lukasz.jakis@email.com','cc03e747a6afbbcbf8be7668acfebee5','314124124242','mbank','463294634982642364846',189342,'0','2009-10-01 00:00:00','2012-07-01 00:00:00',2,0,1,3,'0000-00-00 00:00:00',NULL,'2011-12-15 22:12:59',NULL),(4,'Max','Meksi','87686758','Hollera 124','45-333','Wrocław','maksi.max@email.com','cc03e747a6afbbcbf8be7668acfebee5','22323213313','mbank','34324324343234',9550,'0','2011-12-16 00:10:53','2014-12-16 00:12:53',1,0,1,3,'2011-12-16 00:10:53','rejestracja','2011-12-15 23:10:53',NULL),(5,'Max','Meksi','87686758','Hollera 124','45-333','Wrocław','maksia.max@email.com','cc03e747a6afbbcbf8be7668acfebee5','22323213313','mbank','34324324343234',70580,'0','2011-12-16 00:13:44','2014-12-16 00:12:44',1,0,1,3,'2011-12-16 00:13:44','rejestracja','2011-12-15 23:13:44',NULL),(6,'Maxasaaaaasasaasasasas','Meksisasasasaaaaaaaa','87686758','Hollera 124','45-333','Wrocław','maksias.max@email.com','cc03e747a6afbbcbf8be7668acfebee5','22323213313','mbank','34324324343234',92819,'0','2011-12-16 00:14:40','2014-12-16 00:12:40',4,0,1,3,'2011-12-16 00:14:40','rejestracja','2011-12-15 23:14:40',NULL),(7,'Maxas','Meksis','87686758','Hollera 124','45-333','Wrocław','maksiass.max@email.com','cc03e747a6afbbcbf8be7668acfebee5','22323213313','mbank','34324324343234',96530,'0','2011-12-16 00:15:19','2014-12-16 00:12:19',1,0,1,3,'2011-12-16 00:15:19','rejestracja','2011-12-15 23:15:19',NULL),(8,'Maxass','Meksisadad','87686758','Hollera 124','45-333','Wrocław','makaasiass.max@email.com','cc03e747a6afbbcbf8be7668acfebee5','22323213313','mbank','34324324343234',27987,'0','2011-12-16 00:15:38','2014-12-16 00:12:38',4,0,0,3,'2011-12-16 00:15:38','rejestracja','2011-12-15 23:15:38',NULL),(9,'Jacek','Podpaska','696293629','ul jakas tam','22-222','Wrocław','email1@op.pl','cc03e747a6afbbcbf8be7668acfebee5','4223432423342','mbank','132132323122321322112',0,'XXX001','0000-00-00 00:00:00','0000-00-00 00:00:00',1,2,1,2,'0000-00-00 00:00:00',NULL,'2011-12-16 20:04:46',NULL),(11,'Ewa','Baryla','696293629','ul jakas tam','22-222','Wrocław','email2@op.pl','cc03e747a6afbbcbf8be7668acfebee5','4223432423342','mbank','132132323122321322112',0,'XXX002','0000-00-00 00:00:00','0000-00-00 00:00:00',2,1,1,2,'0000-00-00 00:00:00',NULL,'2011-12-16 20:06:23',NULL),(12,'Marta','Stepien','696293629','ul jakas tam','22-222','Wrocław','email2@op.pl','cc03e747a6afbbcbf8be7668acfebee5','4223432423342','mbank','132132323122321322112',0,'XXX003','0000-00-00 00:00:00','0000-00-00 00:00:00',2,3,1,2,'0000-00-00 00:00:00',NULL,'2011-12-16 20:07:34',NULL),(13,'Jerzy','Kredka','696293629','ul jakas tam','22-222','Wrocław','email2@op.pl','cc03e747a6afbbcbf8be7668acfebee5','4223432423342','mbank','132132323122321322112',0,'XXX004','0000-00-00 00:00:00','0000-00-00 00:00:00',4,1,1,2,'0000-00-00 00:00:00',NULL,'2011-12-16 20:09:06',NULL),(14,'Antek','Mafefka','696293629','ul jakas tam','22-222','Wrocław','email2@op.pl','cc03e747a6afbbcbf8be7668acfebee5','4223432423342','mbank','132132323122321322112',0,'XXX005','0000-00-00 00:00:00','0000-00-00 00:00:00',4,2,1,2,'0000-00-00 00:00:00',NULL,'2011-12-16 20:09:32',NULL),(15,'Adam','Adamski','6701222','Rynek','50-011','WrocÅ‚aw','student@pwr.wroc.pl','8976d7f099397cc85fbd1edec36926b8','606060060','BZWBK','234567',31438,'','2012-01-08 16:12:22','2015-01-08 16:01:05',1,0,1,3,'2012-01-08 16:12:22','rejestracja','2012-01-08 15:12:22',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wydzial`
--

DROP TABLE IF EXISTS `wydzial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wydzial` (
  `id_wydzial` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(333) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_wydzial`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wydzial`
--

LOCK TABLES `wydzial` WRITE;
/*!40000 ALTER TABLE `wydzial` DISABLE KEYS */;
INSERT INTO `wydzial` VALUES (1,'Informatyka'),(2,'Zarzadzanie');
/*!40000 ALTER TABLE `wydzial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zapisy`
--

DROP TABLE IF EXISTS `zapisy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zapisy` (
  `id_zapis` int(11) NOT NULL AUTO_INCREMENT,
  `id_kurs` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `status_zapisu` int(11) NOT NULL,
  `dataZAPIS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dataWYPIS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_zapis`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zapisy`
--

LOCK TABLES `zapisy` WRITE;
/*!40000 ALTER TABLE `zapisy` DISABLE KEYS */;
INSERT INTO `zapisy` VALUES (4,2,3,1,'2011-12-18 12:43:03','0000-00-00 00:00:00'),(5,1,3,0,'2011-12-18 12:52:26','0000-00-00 00:00:00'),(6,2,3,0,'2011-12-18 12:06:31','0000-00-00 00:00:00'),(7,1,3,1,'2011-12-18 12:52:40','0000-00-00 00:00:00'),(8,1,4,0,'2011-12-18 12:54:20','0000-00-00 00:00:00'),(9,2,4,0,'2011-12-18 14:14:34','0000-00-00 00:00:00'),(10,4,15,1,'2012-01-08 15:14:19','0000-00-00 00:00:00'),(11,4,4,1,'2012-01-08 15:16:02','0000-00-00 00:00:00'),(12,4,5,1,'2012-01-08 15:20:26','0000-00-00 00:00:00'),(13,4,7,1,'2012-01-08 15:20:55','0000-00-00 00:00:00'),(14,2,15,1,'2012-01-08 15:27:42','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `zapisy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-25 22:27:16
