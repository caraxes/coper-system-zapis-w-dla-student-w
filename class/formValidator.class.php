﻿<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
	 class formValidator {
		private $errors=array();
		public function __construct(){}
		// validate empty field
		public function validateEmpty($field,$errorMessage,$min=4,$max=32){
			if(!isset($_POST[$field])||trim($_POST[$field])==''||strlen($_POST[$field])<$min||strlen($_POST[$field])>$max){
				$this->errors[]=$errorMessage;
			}
		}
		public function validateCheck($field,$errorMessage){
			if(!isset($_POST[$field])||trim($_POST[$field])==''){
				$this->errors[]=$errorMessage;
			}
		}
		public function validateEmptyGET($field,$errorMessage,$min=4,$max=32){
			if(!isset($_GET[$field])||trim($_GET[$field])==''||strlen($_GET[$field])<$min||strlen($_GET[$field])>$max){
				$this->errors[]=$errorMessage;
			}
		}
		// validate integer field
		public function validateInt($field,$errorMessage){
			if(!isset($_POST[$field])||!is_numeric($_POST[$field])||intval($_POST[$field])!=$_POST[$field]){
				$this->errors[]=$errorMessage;
			}
		}
		// validate numeric field
		public function validateNumber($field,$errorMessage){
			if(!isset($_POST[$field])||!is_numeric($_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		}
		// validate if field is within a range
		public function validateRange($field,$errorMessage,$min=1,$max=99){
			if(!isset($_POST[$field])||$_POST[$field]<$min||$_POST[$field]>$max){
				$this->errors[]=$errorMessage;
			}
		}
		// validate alphabetic field
		public function validateAlphabetic($field,$errorMessage){
			if(!isset($_POST[$field])||!preg_match("/^[a-zA-Z_ęóąśłżźćńĘÓĄŚŁŻŹĆŃ ]+$/",$_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		}
		// validate alphanumeric field
		public function validateAlphanum($field,$errorMessage){
			if(!isset($_POST[$field])||!preg_match("/^[a-zA-Z0-9]+$/",$_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		}
		// validate email
		public function validateEmail($field,$errorMessage){
			if(!isset($_POST[$field])||empty($_POST[$field])||!@eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		} 
		// validate kod-pocztowy
		public function validatZipNumber($field,$errorMessage){
			if(!isset($_POST[$field])||empty($_POST[$field])||!@eregi("[0-9]{2}-[0-9]{3}", $_POST[$field])){
				$this->errors[]=$errorMessage;
			}
		} 
		// validate passwords compare
		public function validateCompare($field,$field2,$errorMessage){
			if(!isset($_POST[$field])||!isset($_POST[$field2])||empty($_POST[$field])||($_POST[$field] != $_POST[$field2])){
				$this->errors[]=$errorMessage;
			}
		}
		// validate if email is set in db
		public function validateMail($field,$pdo,$errorMessage){
			$qu = $pdo->query("SELECT COUNT(id_user) from users WHERE email = '".$_POST[$field]."'");
			if($qu -> fetchColumn() !=0){
				$this->errors[]=$errorMessage;
			}
			$qu->closeCursor();
		}
		// validate if size count is ok in warehouse
		public function validateSize($field,$prod_id,$ilosc,$pdo,$errorMessage){
			if(isset($_POST[$field])) {
				$sql = $pdo->query("SELECT cms_prod_ware_state
										FROM `cms_prod_ware` 
										WHERE id_cms_prod = ".$prod_id." AND id_cms_size = ".$_POST[$field]."");
				
				$size_amount = $sql -> fetch(PDO::FETCH_ASSOC);
				//print_r($size_amount);
				if($size_amount['cms_prod_ware_state'] < $ilosc) $this->errors[]=$errorMessage;
				$sql->closeCursor();
			}
		}

		
		// validate NIP
		/**
		 * Checks whether given string is correct NIP number
		 * Acceptable formats '118-175-57-20', '118 171 57 20', '1181715720'
		 *
		 * @param mixed $nip - NIP number in string or int format
		 * @return boolean
		 * @access public
		 */
		public function validateNIP($nip,$errorMessage) {
			if(isset($_POST[$nip])) $nip = $_POST[$nip];
			else $nip = '';
			if ($nip == 0) { $this->errors[]=$errorMessage; return false; }
			$chr_to_replace = array('-', ' ');		// get rid of these characters
			$nip = str_replace($chr_to_replace, '', $nip);
			if (! is_numeric($nip)) { $this->errors[]=$errorMessage; return false; }
			$weights = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
			$digits = str_split($nip);
			$digits_length = count($digits);
			for ($i = 1; $i < $digits_length; $i++) {
				if ($digits[0] != $digits[$i]) break;
				if ($digits[0] == $digits[$i] && $i == $digits_length - 1) { $this->errors[]=$errorMessage; return false; }
			}//end for
			$in_control_number = intval(array_pop($digits));
			$sum = 0;
			$weights_length = count($weights);
			for ($i = 0; $i < $weights_length; $i++) {
				$sum += $weights[$i] * intval($digits[$i]);
			}//end for
			$modulo = $sum % 11;
			$control_number = ($modulo == 10) ? 0 : $modulo;
				if($in_control_number !== $control_number)
					{ $this->errors[]=$errorMessage; return false; }
		}
		
		//Check PESEL
		public function validatePESEL($pesel,$errorMessage)
		{	
			if(isset($_POST[$pesel])) $pesel = $_POST[$pesel];
			else $pesel = '';
			if (!preg_match('/^[0-9]{11}$/',$pesel)) //sprawdzamy czy ciąg ma 11 cyfr
			{ $this->errors[]=$errorMessage; return false; }
			$arrSteps = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3); // tablica z odpowiednimi wagami
			$intSum = 0;
			for ($i = 0; $i < 10; $i++)
			{
				$intSum += $arrSteps[$i] * $pesel[$i]; //mnożymy każdy ze znaków przez wagę i sumujemy wszystko
			}
			$int = 10 - $intSum % 10; //obliczamy sumę kontrolną
			$intControlNr = ($int == 10)?0:$int;
			if ($intControlNr == $pesel[10]) //sprawdzamy czy taka sama suma kontrolna jest w ciągu
			{
				return true;
			}
			else $this->errors[]=$errorMessage; return false;
		}
		//Check regon
		public function validateREGON($regon,$errorMessage)
		{
			if(isset($_POST[$regon])) $regon = $_POST[$regon];
			else $regon = '';
			if (strlen($regon) != 9)
			{ $this->errors[]=$errorMessage; return false; }
		 
			$arrSteps = array(8, 9, 2, 3, 4, 5, 6, 7);
			$intSum=0;
			for ($i = 0; $i < 8; $i++)
			{
				$intSum += $arrSteps[$i] * $regon[$i];
			}
			$int = $intSum % 11;
			$intControlNr=($int == 10)?0:$int;
			if ($intControlNr == $regon[8]) 
			{
				return true;
			}
		    else $this->errors[]=$errorMessage; return false;
		}/*
		function CheckRegon( $regon ) 
		{
			switch( strlen( $regon ) )
			{
				case 7:
					$weights = array(2, 3, 4, 5, 6, 7);
					break;
				case 9:
					$weights = array(8, 9, 2, 3, 4, 5, 6, 7);
					break;
				case 14:
					$weights = array(2, 4, 8, 5, 0, 9, 7, 3, 6, 1, 2, 4, 8);
					break;
				default:
					return false;
			}
		 
			$checksum = 0;
		 
			foreach( $weights as $i => $w )
			{
				$checksum += $w * $regon[$i];
			}
		 
			return $checksum % 11 % 10 == substr( $regon, -1, 1 );
		}*/
		//Check numer konta
		public function validateNumerNRB($p_iNRB,$errorMessage)
		{
		  // Usuniecie spacji
		  $iNRB = str_replace(' ', '', $p_iNRB);
		  // Sprawdzenie czy przekazany numer zawiera 26 znaków
		  if(strlen($iNRB) != 26)
			{ $this->errors[]=$errorMessage; return false; }
		 
		  // Zdefiniowanie tablicy z wagami poszczególnych cyfr				
		  $aWagiCyfr = array(1, 10, 3, 30, 9, 90, 27, 76, 81, 34, 49, 5, 50, 15, 53, 45, 62, 38, 89, 17, 73, 51, 25, 56, 75, 71, 31, 19, 93, 57);
		 
		  // Dodanie kodu kraju (w tym przypadku dodajemy kod PL)		
		  $iNRB = $iNRB.'2521';
		  $iNRB = substr($iNRB, 2).substr($iNRB, 0, 2); 
		 
		  // Wyzerowanie zmiennej
		  $iSumaCyfr = 0;
		 
		  // Pćtla obliczająca sumć cyfr w numerze konta
		  for($i = 0; $i < 30; $i++) 
			$iSumaCyfr += $iNRB[29-$i] * $aWagiCyfr[$i];
		 
		  // Sprawdzenie czy modulo z sumy wag poszczegolnych cyfr jest rowne 1
		  if($iSumaCyfr % 97 != 1) 
			{ $this->errors[]=$errorMessage; return false; }
		}
		
		// check for errors
		public function checkErrors(){
			if(count($this->errors)>0){
				return true;
			}
			return false;
		}
		// return errors
		public function displayErrors(){
		$errorOutput='Znaleziono błędy';
			$errorOutput.='<ul>';
			foreach($this->errors as $err){
				$errorOutput.='<li><p class="blad" style="color:red">'.$err.'</p></li>';
			}
			$errorOutput.='</ul><br />';
			return $errorOutput;
		}
	}
?>