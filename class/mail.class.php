﻿<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
class mail {

	public function send() {
	
		try 
			{
				
									if($_SERVER['REQUEST_METHOD'] == 'POST') 
									{ 

										$formval = new formValidator();
										$formval -> validateEmpty('imie','Podane imie nie mieści się w przedziale od 3 do 55 znaków',3,55);
										
										$formval -> validateEmpty('comment','Podana treść nie mieści się w przedziale od 10 do 500 znaków',10,500);
										$formval -> validateEmail('email','Podano błędny email ');

										$formval_errors_number = $formval -> checkErrors();
												if($formval_errors_number > 0)
													echo "<br />".$formval -> displayErrors();
						
											if($formval_errors_number == 0) {
												$email=new sendMail(); 
												$email->html();
												$email->from($_POST['email'], 'Nadawca'); 
												$email->add_recipient('biuro@kaliniak.eu');//add a recipient in the to: field  
												$email->add_cc($_POST['email']); // copy to
												$email->subject('Email z formularza kontaktowego ');//set subject 
												$email->message('Imie i nazwisko: '.$_POST['imie'].' tresc: '.$_POST['comment']);//set message body 
												$email->send();//send email(s)
											echo "<br /><p>E-mail został wysłany !</p>";
											unset($_POST);
											}
									}		
								
			}
			catch(PDOException $e) 
			{
			echo 'Błąd, Mail nie został wysłany !';
			}
		
	}
	
	public function form_pracownik() {
	
		try 
			{
				
									if($_SERVER['REQUEST_METHOD'] == 'POST') 
									{ 

										$formval = new formValidator();
										//$formval -> validateEmpty('imie','Podane imie nie mieści się w przedziale od 3 do 55 znaków',3,55);
										
										//$formval -> validateEmpty('comment','Podana treść nie mieści się w przedziale od 10 do 500 znaków',10,500);
										//$formval -> validateEmail('email','Podano błędny email ');

										$formval_errors_number = $formval -> checkErrors();
												if($formval_errors_number > 0)
													echo "<br />".$formval -> displayErrors();
						
											if($formval_errors_number == 0) {
											
											$content = '<div class="registration">
         <p class="registration_p"><label class="registration_label">IMIĘ (imiona):</label> '.$_POST['imie'].'</p>
          <p class="registration_p"><label class="registration_label">NAZWISKO:</label> '.$_POST['nazwisko'].'</p>
          <p class="registration_p"><label class="registration_label">DATA URODZENIA:</label> '.$_POST['data_urodzenia'].'</p>
          <p class="registration_p"><label class="registration_label">OBYWATELSTWO:</label> '.$_POST['obywatel'].'</p>
          <p class="registration_p"><label class="registration_label">MIEJSCE ZAMIESZKANIA:</label> '.$_POST['miejsce_zamieszkania'].'</p>
          <p class="registration_p"><label class="registration_label">ULICA:</label> '.$_POST['ulica'].'<span class="reg_smal">NUMER DOMU:</span> '.$_POST['nr_domu'].'<span class="reg_smal">NUMER MIESZKANIA:</span> '.$_POST['nr_mieszkania'].'</p>
          <p class="registration_p"><label class="registration_label">ULICA:</label> '.$_POST['ulicak'].'<span class="reg_smal">NUMER DOMU:</span> '.$_POST['nr_domuk'].'<span class="reg_smal">NUMER MIESZKANIA:</span> '.$_POST['nr_mieszkaniak'].'</p>
          <p class="registration_p"><label class="registration_label">TEL KOMÓRKOWY:</label> '.$_POST['komorka'].'</p>
          <p class="registration_p"><label class="registration_label">TEL STACJONARNY:</label> '.$_POST['tel'].'</p>
          <p class="registration_p"><label class="registration_label">LICZBA CZŁONKÓW RODZINY NA UTRZYMANIU:</label> '.$_POST['liczba_utrzymanie'].'</p>
          <p class="registration_p">
          	<label class="registration_label">WYKSZTAŁCENIE:</label> ';
          	if(isset($_POST['wyksztalcenie'])) $content.= $_POST['wyksztalcenie'];
          	else $content.=  ' - ';
          	$content.= '
          	
          </p>
          <p class="registration_p"><span class="reg_dod">Charakter pracy do której jest się predysponowanym:</span> '.$_POST['charakter_pracy'].'</p>
           <p class="registration_p"><span class="reg_dod">Specjalność zadowowa wyuczona:</span> '.$_POST['specjalnosc'].'</p>
          <p class="registration_p2"><span class="reg_dod">Poprzednie i obecne miejsca pracy:</span></p>
           <div class="cart">
 <table id="box-table-a">
    <thead>
    	<tr>
        	<th class="tab_nazwa" scope="col">Zakład pracy (siedziba)</th>
        	<th class="tab_nazwa" scope="col">Zakład pracy (siedziba)</th>
            <th class="tab_cena" scope="col">zajmowane stanowisko</th> 
            </tr>
    </thead>
    <tbody>
    		<tr>
        	<td>'.$_POST['zaklad_pracy1'].'</td>
            <td>'.$_POST['stanowisko1'].'</td>
            <td>'.$_POST['okres1'].'</td>
            </tr>	<tr>
        	<td>'.$_POST['zaklad_pracy2'].'</td>
            <td>'.$_POST['stanowisko2'].'</td>
            <td>'.$_POST['okres2'].'</td>
            </tr>	<tr>
        	<td>'.$_POST['zaklad_pracy3'].'</td>
            <td>'.$_POST['stanowisko3'].'</td>
            <td>'.$_POST['okres3'].'</td>
            </tr>	<tr>
        	<td>'.$_POST['zaklad_pracy4'].'</td>
            <td>'.$_POST['stanowisko4'].'</td>
            <td>'.$_POST['okres4'].'</td>
            </tr>
    </tbody>
    
</table>
 </div>
 	<p class="registration_p2"><label class="registration_label">DYSPOZYCYJNOŚĆ:</label>
    ';
          	if(isset($_POST['dyspozycyjnosc'])) $content.=  $_POST['dyspozycyjnosc'];
          	else $content.=  ' - ';
          	$content.= '
    </p>
    ';
    if($_POST['dyspozycyjnosc']!='ograniczona') {
    $content.= '
    <p class="registration_p2"><label class="registration_label">Poniedziałek:</label> '.$_POST['pon'].'</p>
    <p class="registration_p2"><label class="registration_label">Wtorek:</label> '.$_POST['wt'].'</p>
    <p class="registration_p2"><label class="registration_label">Środa:</label> '.$_POST['sr'].'</p>
    <p class="registration_p2"><label class="registration_label">Czwartek:</label> '.$_POST['czw'].'</p>
    <p class="registration_p2"><label class="registration_label">Piątek:</label> '.$_POST['pt'].'</p>
    <p class="registration_p2"><label class="registration_label">Sobota:</label> '.$_POST['sob'].'</p>
    <p class="registration_p"><label class="registration_label">Niedziela:</label> '.$_POST['niedz'].'</p>';
    }
    $content.= '
    <p class="registration_p"><label class="registration_label">PRACA WIELOZMIANOWA:</label> ';
          	if(isset($_POST['praca_wielozmianowa'])) $content.=  $_POST['praca_wielozmianowa'];
          	else $content.=  ' - ';
          	$content.= '
    </p>
        <p class="registration_p"><label class="registration_label">RODZAJ POSZUKIWANEJ PRACY:</label>';
        if(isset($_POST['poszukiwana_praca'])) {
		   foreach($_POST['poszukiwana_praca'] as $p) {
		   	$content.=  $p.', ';
		   }
		} else $content.=  ' - ';
        $content.=  '
    </p>
    <p class="registration_p2"><label class="registration_label">JEZYK ANGIELSKI:</label> ';
          	if(isset($_POST['angielski'])) $content.=  $_POST['angielski'];
          	else $content.=  ' - ';
          	$content.= '
    </p>
    <p class="registration_p2"><label class="registration_label">JEZYK NIEMIECKI:</label> ';
          	if(isset($_POST['niemiecki'])) $content.=  $_POST['niemiecki'];
          	else $content.=  ' - ';
          	$content.= '
    </p>
    <p class="registration_p2"><label class="registration_label">JEZYK HISZPANSKI:</label> ';
          	if(isset($_POST['hiszpanski'])) $content.=  $_POST['hiszpanski'];
          	else $content.=  ' - ';
          	$content.= '
    </p>
    <p class="registration_p"><label class="registration_label">INNY:</label> '.$_POST['jezyk_inny'].'</p>
    <p class="registration_p">Dodatkowe umiejętności:</p>
    <p class="registration_p"><label class="registration_label">PRAWO JAZDY:</label> ';
          	if(isset($_POST['prawko'])) $content.=  $_POST['prawko'];
          	else $content.=  ' - ';
          	$content.= '
    </p>
   	<p class="registration_p"><label class="registration_label">KURSY (np.wózek widłowy):</label> '.$_POST['kursy'].'</p>
  	<p class="registration_p"><label class="registration_label">OBSŁUGA URZĄDZEŃ BIUROWYCH:</label> '.$_POST['obsluga_urzadzen'].'</p>
    <p class="registration_p"><label class="registration_label">OBSŁUGA KOMPUTERA:</label> '.$_POST['obsluga_komputera'].'</p>
    <p class="registration_p"><label class="registration_label">INNE (np. badania sanepidu):</label> '.$_POST['obsluga_inne'].'</p>
    <p class="registration_p2"><label class="registration_label">BRANŻA:</label> ';
          	if(isset($_POST['branza'])) $content.=  $_POST['branza'];
          	else $content.=  ' - ';
          	$content.= '
    </p>
    <p class="registration_p"><label class="registration_label">inne:</label> '.$_POST['branza_inne'].'</p>
    <p class="registration_p"><label class="registration_label">ZAINTERESOWANIA POZAZAWODOWE</label>
     '.$_POST['zainteresowania'].'
    </p>
        <p class="registration_p"><label class="registration_label">CECHY OSOBOWOŚCI</label>
     '.$_POST['cechy'].'
    </p>
    
	 	</div> ';
								//echo $content;			
											
												$email=new sendMail(); 
												$email->html();
												$email->from('biuro@kaliniak.eu', 'biuro@kaliniak.eu'); 
												$email->add_recipient('biuro@kaliniak.eu');//add a recipient in the to: field  
												//$email->add_cc($_POST['email']); // copy to
												$email->subject('Kwestionariusz osobowy ');//set subject 
												$email->message($content);//set message body 
												$email->send();//send email(s)
											echo "<br /><p>Kwestionariusz osobowy został wysłany !</p>";
											//unset($_POST);
											}
									}		
								
			}
			catch(PDOException $e) 
			{
			echo 'Błąd, Mail nie został wysłany !';
			}
		
	}
	
	public function form_pracodawca() {
	
		try 
			{
				
									if($_SERVER['REQUEST_METHOD'] == 'POST') 
									{ 

										$formval = new formValidator();
										//$formval -> validateEmpty('imie','Podane imie nie mieści się w przedziale od 3 do 55 znaków',3,55);
										
										//$formval -> validateEmpty('comment','Podana treść nie mieści się w przedziale od 10 do 500 znaków',10,500);
										//$formval -> validateEmail('email','Podano błędny email ');

										$formval_errors_number = $formval -> checkErrors();
												if($formval_errors_number > 0)
													echo "<br />".$formval -> displayErrors();
						
											if($formval_errors_number == 0) {
											
											$content = '<div class="registration">
            <p class="registration_p"><label class="registration_label">NAZWA FIRMY:</label> '.$_POST['nazwa'].'</p>
            <p class="registration_p"><label class="registration_label">ADRES FIRMY:</label> '.$_POST['adres'].'</p>
            <p class="registration_p"><label class="registration_label">NR TELEFONU:</label> '.$_POST['tel'].'</p>
            <p class="registration_p"><label class="registration_label">NR FAKSU:</label> '.$_POST['fax'].'</p>
            <p class="registration_p"><label class="registration_label">NIP:</label> '.$_POST['nip'].'</p>
            <p class="registration_p"><label class="registration_label">REGON:</label> '.$_POST['regon'].'</p>
            <p class="registration_p2"><span class="reg_dod2">Forma zatrudnienia proponowana przez Agencje Pracy Tymczasowej ,,KALINIAK’’:</span></p>'.$_POST['forma'].'  <p class="registration_p2"><label class="registration_label">ZAPOTRZEBOWANIE PRACOWNIKÓW:</label> '.$_POST['il_pracownikow'].'</p>
            <p class="registration_p2"><label class="registration_label">ilość kobiet:</label> '.$_POST['ile_kobiet'].'</p>
            <p class="registration_p"><label class="registration_label">ilość mężczyzn:</label> '.$_POST['ile_men'].'</p>
            <p class="registration_p"><label class="registration_label">OKRES ZATRUDNIENIA PRACOWNIKÓW:</label> '.$_POST['okres'].'</p>
            <p class="registration_p2"><label class="registration_label">DOCELOWE MIEJSCE PRACY:</label> '.$_POST['miejsce'].'</p>
            <p class="registration_p"><label class="registration_label">SZCZEGÓŁY CZASU PRACY:</label> '.$_POST['szczegoly'].'</p>
            <p class="registration_p"><label class="registration_label">WYKSZTAŁCENIE:</label> '.$_POST['wyksztalcenie'].'</p>
            <p class="registration_p"><label class="registration_label">WYNAGRODZENIE GODZINOWE:</label> '.$_POST['zah'].'</p>
            <p class="registration_p"><label class="registration_label">WYNAGRODZENIE MIESIĘCZNE:</label> '.$_POST['zam'].'</p>
            <p class="registration_p"><label class="registration_label">REALIZACJA OFERTY DO DNIA:</label> '.$_POST['kiedy'].'</p>
        </div>';
								//echo $content;			
									
												$email=new sendMail(); 
												$email->html();
												$email->from('biuro@kaliniak.eu', 'biuro@kaliniak.eu'); 
												$email->add_recipient('biuro@kaliniak.eu');//add a recipient in the to: field  
												//$email->add_cc($_POST['email']); // copy to
												$email->subject('Oferta pracodawcy ');//set subject 
												$email->message($content);//set message body 
												$email->send();//send email(s)
											echo "<br /><p>Kwestionariusz osobowy został wysłany !</p>";
											//unset($_POST);
											}
									}		
								
			}
			catch(PDOException $e) 
			{
			echo 'Błąd, Mail nie został wysłany !';
			}
		
	}

}
	
?>
