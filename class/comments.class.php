<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
	class comments {
	 
	  protected $host;
	  protected $user;
	  protected $pwd;
	  protected $dbName;
	 
		 function __construct($host, $user, $pwd, $dbName){
			$this->host = $host;
			$this->user = $user;
			$this->pwd = $pwd;
			$this->dbName = $dbName;
		}
		
		
		public function getComments() {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT * FROM comment
								INNER JOIN users ON comment.id_dziekanat = users.id_user
								
								WHERE users.status=1 AND users.type=1 ORDER BY comment.created_at DESC");
			$data = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		public function editComments() {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				try
				{
		
					//DATA VERIFICATION:
					$formval = new formValidator();
		
		
					$formval -> validateEmpty('opis',"Podaj jakiś opis",3,200);
						
						
					$formval_errors_number = $formval -> checkErrors();
					if($formval_errors_number > 0)
					echo $formval -> displayErrors();
		
					//DATA VERIFICATION end:
		
					$sql = $pdo -> prepare("INSERT INTO `comment` (`id_dziekanat`, `description`, `status`, `created_at`, `updated_at`)
					 VALUES ('".$_SESSION['user_id']."', :desc, '1', NOW(), '');");
						
					//$sql -> bindParam(':iddziek', $_SESSION['user_id'], PDO::PARAM_INT, 1);
					$sql -> bindParam(':desc', $_POST['opis'], PDO::PARAM_STR, 100);
					
		
					if($formval_errors_number == 0) {
						$sql -> execute();
						//print_R($sql->errorInfo());
						$sql->closeCursor();
						//die();
						echo "wpis dodano !";
							
							
						echo "<script>setTimeout ( \"document.location = 'dziekanat'\",1000)</script>";
							
					}
		
		
				}
				catch(PDOException $e)
				{
					echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
				}
			}
				
		}
		
		
	}
	 

	 
?>
