<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
	class kurs {
	 
	  protected $host;
	  protected $user;
	  protected $pwd;
	  protected $dbName;
	 
		 function __construct($host, $user, $pwd, $dbName){
			$this->host = $host;
			$this->user = $user;
			$this->pwd = $pwd;
			$this->dbName = $dbName;
		}
		
		public function addKurs() {
			try 
			{
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			
				if($_SERVER['REQUEST_METHOD'] == 'POST') 
				{ 	
					try
				   {
									

					//DATA VERIFICATION:  
					  
					$formval = new formValidator();
					
					
						$formval -> validateEmpty('nazwa',"Podaj nazwe kursu  (min 3 znaki)",3,100);
						//$formval -> validateAlphabetic('nazwa','Nazwa moze skladac sie tylko z liter');
						$formval -> validateEmpty('opis',"Podaj opis (min 3 znaki)",3,200);
						$formval -> validateEmpty('ects',"Podaj ilosc ECTS",1,200);
						$formval -> validateAlphanum('ects',"ECTS - tylko liczby !");
						$formval -> validateEmpty('kod_kursu',"Podaj kod kursu  (min 3 znaki)",3,100);
						$formval -> validateEmpty('kod_grupy',"Podaj kod grupy  (min 3 znaki)",3,100);
						$formval -> validateEmpty('termin',"Podaj termin  (min 3 znaki)",3,200);
						

						$formval_errors_number = $formval -> checkErrors();
							if($formval_errors_number > 0)
								echo $formval -> displayErrors();
							
							
							
					//die($_POST['rodzaj']);
					   $sql = $pdo -> prepare("INSERT INTO kursy (
								`id_prowadzacy` ,
								`nazwa` ,
								`opis` ,
								`ects` ,
								`forma_zaliczenia` ,
								`kod_kursu` ,
								`kod_grupy` ,
								`termin` ,
								`miejsca` ,
								`miejsca_minimum` ,
								`platny` ,
								`ile_platny` ,
								`status` ,
								`date_add` ,
								`login_add`
								) VALUES (
								:id_prowadzacy,
								:nazwa,
								:opis,
								:ects,
								:forma_zaliczenia,
								:kod_kursu,
								:kod_grupy,
								:termin,
								:miejsca,
								:miejsca_minimum,
								:platny,
								:ile_platny,
								1,
								NOW(),
								'dodawanie')");
					
					   $sql -> bindParam(':id_prowadzacy', $_POST['prowadzacy'], PDO::PARAM_INT, 3);
					  $sql -> bindParam(':nazwa', $_POST['nazwa'], PDO::PARAM_STR, 105);
					  $sql -> bindParam(':opis', $_POST['opis'], PDO::PARAM_STR, 385);
					  $sql -> bindParam(':ects', $_POST['ects'], PDO::PARAM_INT, 11);
					  $sql -> bindParam(':forma_zaliczenia', $_POST['forma_zaliczenia'], PDO::PARAM_INT, 10);
					  $sql -> bindParam(':kod_kursu', $_POST['kod_kursu'], PDO::PARAM_STR, 16);
					  $sql -> bindParam(':kod_grupy', $_POST['kod_grupy'], PDO::PARAM_STR, 145);
					  $sql -> bindParam(':termin', $_POST['termin'], PDO::PARAM_STR, 245);
					  $sql -> bindParam(':miejsca', $_POST['miejsca'], PDO::PARAM_INT, 5);
					  $sql -> bindParam(':miejsca_minimum', $_POST['miejsca_minimum'], PDO::PARAM_INT, 5);
					  $sql -> bindParam(':platny', $_POST['platny'], PDO::PARAM_INT, 5);
					  $sql -> bindParam(':ile_platny', $_POST['ile_platny'], PDO::PARAM_INT, 5);
					
				//print_R($sql); die();	
					   if($formval_errors_number == 0) {
							$sql -> execute();
							
							//print_R($sql->errorInfo());
							//die();
							$sql->closeCursor();
							
							echo "Dodano nowy kurs !<br />";
							echo "<script>setTimeout ( \"document.location = 'dziekanat_kursy'\",1000)</script>";
								
							
					   }
				   }
				   catch(PDOException $e)
				   {
					  echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
				   }
				}
			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function delKurs($id) {
			try 
			{
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				
				$sql = $pdo -> exec("UPDATE `kursy` SET status = 0 WHERE id_kurs = ".$id);
				   
			}	   
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function checkSpaceInKurs($id_kursu) {
			try
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			
				$sql = $pdo->query("SELECT count(*) as `all` FROM zapisy WHERE status_zapisu =1 AND id_kurs = ".$id_kursu);
				$kw = $sql->fetch(PDO::FETCH_ASSOC);
				
				$sql = $pdo->query("SELECT miejsca FROM kursy WHERE id_kurs = ".$id_kursu);
				$kmax = $sql->fetch(PDO::FETCH_ASSOC);
				
				if($kmax['miejsca']>=$kw['all']) $wolne = TRUE;
				else $wolne = FALSE;
				
				return $wolne;
				
			}
			catch(PDOException $e)
			{
				echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function returnSpaceInKurs($id_kursu) {
			try
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
					
				$sql = $pdo->query("SELECT count(*) as `all` FROM zapisy WHERE status_zapisu =1 AND id_kurs = ".$id_kursu);
				$kw = $sql->fetch(PDO::FETCH_ASSOC);
		
				$sql = $pdo->query("SELECT miejsca FROM kursy WHERE id_kurs = ".$id_kursu);
				$kmax = $sql->fetch(PDO::FETCH_ASSOC);
				
				if(($kmax['miejsca']-$kw['all']) <=1) return '0';
				else return (int)($kmax['miejsca']-$kw['all']);
				
		
			}
			catch(PDOException $e)
			{
				echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function returnBizzySpaceInKurs($id_kursu) {
			try
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
					
				$sql = $pdo->query("SELECT count(*) as `all` FROM zapisy WHERE status_zapisu =1 AND id_kurs = ".$id_kursu);
				$kw = $sql->fetch(PDO::FETCH_ASSOC);

				return $kw['all'];
		
		
			}
			catch(PDOException $e)
			{
				echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function zapiszNaKurs($id_kursu, $id_studenta) {
			try
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				
				//sprawdz czy jest juz zapisany
				$sql = $pdo->query("SELECT * FROM zapisy WHERE id_kurs = ".$id_kursu." AND id_student =".$id_studenta." AND status_zapisu=1 OR status_zapisu=2");
				$czy_zapisany = $sql->fetch(PDO::FETCH_ASSOC);
				
				if(!empty($czy_zapisany)) return array('status'=> FALSE, 'why' => 'Już sie tu zapisałeś !');
				
				//sprawdz wolne miejsca
				$wolne = $this->checkSpaceInKurs($id_kursu);
				
				//sprawdz czy platne
				$sql = $pdo->query("SELECT * FROM kursy WHERE kursy.status=1 AND kursy.id_kurs = ".$id_kursu);
				$kurs = $sql->fetch(PDO::FETCH_ASSOC);
		
				
				if($wolne){
					
					if($kurs['platny']=='1') $status = '2'; // do zatwierdzenia przez pd
					else $status = '1';
					
					$sql = $pdo -> exec("INSERT INTO `zapisy` (`id_kurs`, `id_student`, `status_zapisu`, `dataZAPIS`, `dataWYPIS`) 
										VALUES ('".$id_kursu."', '".$id_studenta."', '".$status."', CURRENT_TIMESTAMP, '0000-00-00 00:00:00');");
					return array('status'=> TRUE, 'why' => 'Zapisano !');
				} else {
					return array('status'=> FALSE, 'why' => 'Brak miejsc !');
				}
				
				
				return array('status'=> FALSE, 'why' => 'Blad systemu !');
					
			}
			catch(PDOException $e)
			{
				echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function WypiszZKursu($id) {
			try
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
		
				$sql = $pdo -> exec("UPDATE `zapisy` SET status_zapisu = 0 WHERE id_zapis = ".$id);
				
				return array('status'=> TRUE, 'why' => 'Wypisano !');
			}
			catch(PDOException $e)
			{
				echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function getAllKursy() {
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				$sql = $pdo->query("SELECT * FROM kursy 
				INNER JOIN users ON kursy.id_prowadzacy = users.id_user
				
				WHERE users.status=1 AND users.type=2 AND kursy.status=1");
				$data = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $data;
				
		}
		
		public function getKurs($id) { 
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				$sql = $pdo->query("SELECT * FROM kursy 
				INNER JOIN users ON kursy.id_prowadzacy = users.id_user
				
				WHERE users.status=1 AND users.type=2 AND kursy.status=1 AND kursy.id_kurs =".$id); 
				$data = $sql->fetch(PDO::FETCH_ASSOC);
				return $data;
				
		}
		
		public function getAllZapisaneKursy($id_student) {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT *  FROM zapisy
						INNER JOIN kursy ON zapisy.id_kurs = kursy.id_kurs
						INNER JOIN users ON kursy.id_prowadzacy = users.id_user
						
						WHERE users.status=1 AND users.type=2 AND kursy.status=1 AND zapisy.status_zapisu >=1 AND zapisy.id_student =".$id_student); 
			$data = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		public function getAllWykladowcy() {
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				$sql = $pdo->query("SELECT *, specjalnosc.nazwa as specjalnosc_nazwa, wydzial.nazwa as wydzial_nazwa, stopien.nazwa as stopien_nazwa FROM users 
				INNER JOIN specjalnosc ON users.id_specjalnosc = specjalnosc.id_specjalnosc
				INNER JOIN wydzial ON specjalnosc.id_wydzial = wydzial.id_wydzial
				INNER JOIN stopien ON users.id_stopien 	= stopien.id_stopien
				WHERE users.status=1 AND users.type=2");
				$data = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $data;
				
		}
		
		public function getSpecjalnosci() {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT * FROM specjalnosc");
			$data = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		
		public function editKurs($id) {
		
		$user_data = array();
		
		if(!isset($_SESSION['user_id'])||!is_numeric($_SESSION['user_id']))
			die ("Błąd Krytyczny, brak odpowiednich danych, może nie jesteś zalogowany.");
			
			try 
			{
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			
				
				
				if($_SERVER['REQUEST_METHOD'] == 'POST') 
				{ 	
					try
				   {
					
					//DATA VERIFICATION:  
					$formval = new formValidator();
					
					
						$formval -> validateEmpty('nazwa',"Podaj nazwe kursu  (min 3 znaki)",3,100);
						//$formval -> validateAlphabetic('nazwa','Nazwa moze skladac sie tylko z liter');
						$formval -> validateEmpty('opis',"Podaj opis (min 3 znaki)",3,200);
						$formval -> validateEmpty('ects',"Podaj ilosc ECTS",1,200);
						$formval -> validateAlphanum('ects',"ECTS - tylko liczby !");
						$formval -> validateEmpty('kod_kursu',"Podaj kod kursu  (min 3 znaki)",3,100);
						$formval -> validateEmpty('kod_grupy',"Podaj kod grupy  (min 3 znaki)",3,100);
						$formval -> validateEmpty('termin',"Podaj termin  (min 3 znaki)",3,200);
						

						$formval_errors_number = $formval -> checkErrors();
							if($formval_errors_number > 0)
								echo $formval -> displayErrors();
							
					//DATA VERIFICATION end: 
					
					  $sql = $pdo -> prepare("UPDATE `kursy` SET 
					  `id_prowadzacy` = :id_prowadzacy,
					  `nazwa` = :nazwa,
					  `opis` = :opis,
					  `ects` = :ects,
					  `forma_zaliczenia` = :forma_zaliczenia,
					  
					  `kod_kursu` =  :kod_kursu,
					  `kod_grupy` = :kod_grupy,
					  `termin` = :termin,
					  `miejsca` =  :miejsca,
					  `miejsca_minimum` = :miejsca_minimum,
					  `platny` = :platny,
					  `ile_platny` = :ile_platny
						WHERE id_kurs = '".$id."' ");

					   $sql -> bindParam(':id_prowadzacy', $_POST['prowadzacy'], PDO::PARAM_INT, 3);
					  $sql -> bindParam(':nazwa', $_POST['nazwa'], PDO::PARAM_STR, 105);
					  $sql -> bindParam(':opis', $_POST['opis'], PDO::PARAM_STR, 385);
					  $sql -> bindParam(':ects', $_POST['ects'], PDO::PARAM_INT, 11);
					  $sql -> bindParam(':forma_zaliczenia', $_POST['forma_zaliczenia'], PDO::PARAM_INT, 10);
					  $sql -> bindParam(':kod_kursu', $_POST['kod_kursu'], PDO::PARAM_STR, 16);
					  $sql -> bindParam(':kod_grupy', $_POST['kod_grupy'], PDO::PARAM_STR, 145);
					  $sql -> bindParam(':termin', $_POST['termin'], PDO::PARAM_STR, 245);
					  $sql -> bindParam(':miejsca', $_POST['miejsca'], PDO::PARAM_INT, 5);
					  $sql -> bindParam(':miejsca_minimum', $_POST['miejsca_minimum'], PDO::PARAM_INT, 5);
					  $sql -> bindParam(':platny', $_POST['platny'], PDO::PARAM_INT, 5);
					  $sql -> bindParam(':ile_platny', $_POST['ile_platny'], PDO::PARAM_INT, 5);
					  
					  
					   if($formval_errors_number == 0) {
							$sql -> execute();
							//print_R($sql->errorInfo());
							$sql->closeCursor();
							//die();
							echo "dane zmieniono !";
							
							
							echo "<script>setTimeout ( \"document.location = 'dziekanat_kursy_edytuj?id=".$id."'\",1000)</script>";
							
						}
					  
					  
				   }
				   catch(PDOException $e)
				   {
					  echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
				   }
				}
			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		
		public function zaplacZaKurs($id_kursu, $id_studenta) {
			try
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
		
				$sql = $pdo -> exec("INSERT INTO `oplaty` (
		`id_kurs` ,
		`id_student` ,
		`id_dziekanat` ,
		`id_status_oplat` ,
		`data_dodania` ,
		`data_operacji_koncowej`
		)
		VALUES (
		 '".$id_kursu."', '".$id_studenta."', '0', '0', NOW(), ''
		);");
		
				$sql = $pdo -> exec("UPDATE `zapisy` SET status_zapisu = 3 WHERE id_kurs = ".$id_kursu." AND status_zapisu=2 AND id_student = ".$id_studenta);
		
		
				return array('status'=> TRUE, 'why' => 'Zaplacono !');
			}
			catch(PDOException $e)
			{
				echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function getAllOplaty() {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT * FROM oplaty
						INNER JOIN users ON oplaty.id_student = users.id_user
						INNER JOIN kursy ON oplaty.id_kurs = kursy.id_kurs
						
						WHERE users.status=1 AND users.type=3 AND kursy.status=1");
			$data = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		public function getOplaty($id) {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT * FROM oplaty
								INNER JOIN users ON oplaty.id_dziekanat = users.id_user
								INNER JOIN kursy ON oplaty.id_kurs = kursy.id_kurs
								
								WHERE users.status=1 AND users.type=1 AND kursy.status=1 AND oplaty.id_student=".$id);
			$data = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		public function getIdZapis($id_kurs, $id_student) {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT * FROM zapisy
								WHERE id_kurs=".$id_kurs." AND id_student=".$id_student." AND status_zapisu=3");
			$data = $sql->fetch(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		public function zmienStatusOplaty($id, $s, $id_zapis) {
			try
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			
				$sql = $pdo -> exec("UPDATE `oplaty` SET id_status_oplat = ".$s.", id_dziekanat = ".$_SESSION['user_id']." WHERE id_oplata = ".$id);
				
				if($s=='2') $my_s = '4';
				else $my_s = '1';
				
				$sqll = $pdo -> exec("UPDATE `zapisy` SET status_zapisu = ".$my_s." WHERE id_zapis = ".$id_zapis);
				
				return array('status'=> TRUE, 'why' => 'Zmieniono status !');
			}
			catch(PDOException $e)
			{
				echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function getDaty() {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT * FROM daty_zapisow");
			$data = $sql->fetch(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		
		public function editDaty() {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				try
				{
						
					//DATA VERIFICATION:
					$formval = new formValidator();
						
						
					$formval -> validateEmpty('od',"Podaj zakres startu (format daty 2011-12-18 15:30:00)",18,20);
					$formval -> validateEmpty('do',"Podaj zakres końca (format daty 2011-12-18 15:30:00)",18,20);
					
			
					$formval_errors_number = $formval -> checkErrors();
					if($formval_errors_number > 0)
					echo $formval -> displayErrors();
						
					//DATA VERIFICATION end:
						
					$sql = $pdo -> prepare("UPDATE `daty_zapisow` SET
								  `od` =  :od,
								  `do` = :do");
			
					$sql -> bindParam(':od', $_POST['od'], PDO::PARAM_STR, 30);
					$sql -> bindParam(':do', $_POST['do'], PDO::PARAM_STR, 30);
											
						
					if($formval_errors_number == 0) {
						$sql -> execute();
						//print_R($sql->errorInfo());
						$sql->closeCursor();
						//die();
						echo "date zmieniono !";
							
							
						echo "<script>setTimeout ( \"document.location = 'dziekanat_zapisy_daty'\",1000)</script>";
							
					}
						
						
				}
				catch(PDOException $e)
				{
					echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
				}
			}
			
		}
		
		
		
		
		public function getAllKursyWykladowcy() {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT * FROM kursy
						INNER JOIN users ON kursy.id_prowadzacy = users.id_user
						
						WHERE users.status=1 AND users.type=2 AND kursy.status=1 AND kursy.id_prowadzacy=".$_SESSION['user_id']);
			$data = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		
	}
	 

	 
?>
