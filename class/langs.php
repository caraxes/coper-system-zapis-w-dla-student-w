<?php
if(isset($_SESSION['l'])) { // 1 = pl, 2 = eng, 3 = de
	if($_SESSION['l'] == '1') {
		define('ZNAJDUJESZ_SIE', 'Znajdujesz się w');
		define('ZALOGOWANY_JAKO', 'Jestes zalogowany jako:');
		
		define('WYLOGUJ', 'wyloguj sie');
		define('MAIL_WYSLANY', 'Email został wysłany !');
		define('DANE_ZMENIONO', 'Dane zmieniono');
		define('KOSZYK_SUKCES', 'Produkt został pomyślnie dodany do koszyka !');
		define('NO_ORDER' ,'Nie dokonałeś jeszcze zakupów.');
		define('KOSZYK_PUSTY', 'W koszyku jest pusto, zapraszamy do zakupów');
		define('IMIE_NAZWISKO', 'Imię i Nazwisko');
		define('TRESC', 'Treść');
		define('WYSLIJ', 'Wyślij');
		define('EDYTUJ', 'Edytuj');
		define('ZMIEN', 'Zmień');
		define('TUTAJ', 'tutaj');
		define('USUN', 'Usuń');
		define('DODAJ', 'Dodaj');
		define('CENA', 'Cena');
		
		define('ZAKONCZ', 'Zakończ');
		define('ZASTRZEZONE', 'Zastrzeżony');
		define('WROC', 'Wróć');
		define('PODSUMOWANIE', 'Podsumowanie');
		define('HASLO', 'Hasło');
		define('LOGUJ', 'Zaloguj');
		define('BRAK_KONTA', 'brak konta');
		define('NOWE_KONTO', 'nowe konto');
		define('MAIL_ZAJETY', 'Wybrany email jest już zajęty');
		define('PRZEGLADARKA', 'Przeglądarka');
		
		// form ver
		define('ZAKRES_IMIE', 'Podane imię nie mieści się w zakresie (od 3 do 55 znaków)');
		define('ZAKRES_TRESC', 'Podana treść nie mieści się w zakresie (od 3 do 500 znaków)');
		define('ZAKRES_HASLO', 'Podane hasło musi mieć więcej niż 5 znaków');
		define('EMAIL_BLAD', 'Podany email jest błędny');
		define('BLAD_LOGOWANIA', 'Błąd logowania - złe dane');
		define('KOSZYK_ILOSC_BLAD', 'Podana ilość musi być wyrazona cyfrą !');
		define('KOSZYK_ILOSC_ZADUZO', 'Podana ilość towaru jest większa niż dostępna w magazynie !');
		define('IMIE_PUSTE', 'Podane imię nie mieści się w zakresie (od 3 do 55 znaków)');
		define('IMIE_BLAD', 'W podanym imieniu mogą występować tylko litery i znak _ !');
		define('NAZWISKO_PUSTE', 'Podane nazwisko nie mieści się w zakresie (od 3 do 55 znaków)');
		define('NAZWISKO_BLAD', 'W podanym nazwisku mogą występować tylko litery i znak _ !');
		define('NAZWA_FIRMY_BLAD', 'Nazwa firmy musi mieć min 3 znaki');
		define('NIP_BLAD', 'NIP musi zawierać 10 liczb, znaki - są dozwolone');
		define('PESEL_BLAD', 'PESEL musi zawierać 11 liczb');
		define('ADRES_PUSTE', 'Proszę podać dłuższy adres');
		define('REGON_BLAD', 'Zły regon');
		define('KOD_BLAD', 'Zły kod pocztowy');
		define('MIASTO_PUSTE', 'Podane miasto nie mieści się w zakresie (od 3 do 145 znaków)');
		define('MIASTO_BLAD', 'W podanym mieście mogą występować tylko litery i znak _ !');
		define('HASLA_BLAD', 'Podane hasła nie są takie same !');
		define('BLAD_DOM', 'Numer telefonu domowego może zawierać tylko cyfry !');
		define('BLAD_KOM', 'Numer telefonu komórkowego może zawierać tylko cyfry !');
		define('BANK_BLAD', 'Podana nazwa banku nie mieści się w zakresie (od 3 do 245 znaków)');
		define('NR_KONTA_BLAD', 'Podany numer konta bankowego nie zgadza się ze standardem NRB');
		define('TRESC_BLAD' ,'Dodaj treść do wpisu ');
		define('KSIEGA_SUKCES', 'Wpis dodano !');
		define('KOM_RATE', 'Wystaw komentarz i oceń produkt');
		define('OCEN_PRODUKT', 'Proszę ocenić produkt !');
		
		// menu
		define('TWOJA_STRONA', 'Twoja strona');
		define('OFERTA', 'Oferta');
		define('BUKIET', 'Bukiet');
		define('KSIEGA_GOSCI', 'Księga gości');
		define('KOSZYK', 'Koszyk');
		
		define('LOGOWANIE', 'Logowanie');
		define('EDYCJA_DANYCH', 'Edycja danych');
		define('DODAWANIE_USERA', 'Tworzenie nowego konta');
		define('ZMIANA_HASLA', 'Zmiana hasła');
		define('KOSZYK_POKAZ', 'Pokaż koszyk');
		define('HISTORY_LOG', 'Historia logowań');
		define('HISTORY_ZAM', 'Historia zamówień');
		define('INFO_TWOJA_STRONA', 'Witaj w panelu administracyjnym ! Z tąd możesz poprawić swoje dane, zmienić hasło do konta, przeglądać koszyk, oraz zagłębić się w historie logowań i swoich zamówień.');
		define('DODAJ_USERA_INFO', 'Witaj w dziale dodawania nowego użytkownika ! <br /> * - pola wymagane');
		define('EDYCJA_DANYCH_INFO', 'Witaj w dziale edytowania swoich danych !');
		
		//pola form
		define('IMIE', 'Imię');
		define('NAZWISKO', 'Nazwisko');
		define('PESEL', 'PESEL');
		define('NAZWA_FIRMY', 'Nazwa firmy');
		define('NIP', 'NIP');
		define('REGON', 'REGON');
		define('ULICA', 'Ulica');
		define('KOD_POCZ', 'Kod pocztowy');
		define('MIASTO', 'Miasto');
		define('EMAIL', 'Email');
		
		define('HASLO_POW', 'Powtórz hasło');
		define('TEL_KOM', 'Telefon komórkowy');
		define('TEL_DOM', 'Telefon domowy');
		define('NAZWA_BANKU', 'Nazwa banku');
		define('NR_KONTA', 'Numer konta');
		define('OF', 'Osoba fizyczna');
		define('FIRMA', 'Firma');
		define('REJESTRUJESZ_SIE_JAKO', 'Rejestrujesz się jako');
		
		//twoja strona
		define('DATA_LOGOWANIA', 'Data logowania');
		
		define('HASLO_ZMIENIONE', 'Hasło zmienione !');
		define('HASLO_BLAD', 'Stare hasło nie pokrywa się z wprowadzonym');
		
		define('HASLO_VAL_OLD', 'Stare hasło musi mieć więcej niż 5 znaków');
		define('HASLO_NEW_CHARS', 'Nowe hasło musi mieć więcej niż 5 znaków');
		define('HASLO_VAL_NEW', 'Nowo podane hasła nie są takie same !');
		
		define('HASLO_STARE', 'Stare hasło');
		define('HASLO_NEW', 'Nowe hasło');
		define('HASLO_NEW_REPLY', 'Powtórz nowe hasło');
		
		define('NR_ZAMOWIENIA', 'nr zamówienia');
		define('SZCZEGOLY', 'szczegóły');
		define('Z_DNIA', 'z dnia');
		define('O_GODZINIE', 'o godzinie');
		
		define('SZCZEGOLY_ORDER_TEXT', 'Szczegóły zamówienia nr');

		//tabelka
		define('NAZWA_PRODUKTU' ,'Nazwa produktu');
		define('ILOSC' ,'Ilość');
		define('RAZEM' ,'Razem');
		define('WSUMIE' ,'W sumie');
		define('ROZMIAR', 'Rozmiar');
		define('WYBIERZ_KURIERA' ,'Wybierz kuriera');
		define('KURIER_INFO' ,'Oto lista dostępnych kurierów dla 
tego zamówienia, proszę wybrać opcje która państwu najlepiej pasuje:');
		
		define('WERYFIKCJA', 'Weryfikacja');
		define('WERYFIKCJA_OK', 'Oto towar który planujesz kupić - proszę sprawdzić czy dane są poprawne i zatwierdzić');
		define('WERYFIKCJA_WROC', 'jeśli wszystko sie zgadza lub poprawić');
		
		define('ORDER_OK_MAIL', 'Na twoja skrzynke pocztową zostal wysłany email podany przy rejestracji z podobną treścią');
		
		define('WYLOGOWANIE_OK', 'Wylogowanie zakończone sukcesem !');
		define('NIEMA_SWIAT', 'Dzisiaj nie obchodzimy żadnego swięta');
		define('CURIER_ERROR_LOGIN', 'Błąd ! - Aby dokonać zamówienia trzeba być zalogowanym, jesli nie masz konta możesz je bezpłatnie założyć');
		define('DATA_DODANIA', 'Data dodania');
		define('ZNALEZIONO_BLEDY', 'Znaleziono błędy');
		define('USUNIETO', 'Usunieto !');
		define('OTO_KUPIONY_TOWAR', 'Dziękujemy za złożenie zamówienia, Twoje zamówienie zostało przekazane do realizacji. Na skrzynkę e-mail otrzymasz potwierdzenie złożonego zamówienia.<br /> Zapraszamy do kontynuowania zakupów.');
		define('ORDER_DESC', 'Jeśli masz jakieś zastrzeżenia opisz je poniżej');
		define('BRAK_KURIERA', 'Nie ma takiego kuriera ! Prosze wybrać jeszcze raz innego kuriera');
		define('KOLOR', 'Kolor');
	}
	elseif($_SESSION['l'] == '2') {
		define('ZNAJDUJESZ_SIE', 'You are in');
		define('ZALOGOWANY_JAKO', 'Signed in as:');
		define('WYLOGUJ', 'Sign Out');
		define('MAIL_WYSLANY', 'Email sent !');
		define('DANE_ZMENIONO', 'Changes saved');
		define('KOSZYK_SUKCES', 'Product has been added to your basket !');
		define('NO_ORDER' ,'No order has been placed.');
		define('KOSZYK_PUSTY', 'No items in your basket, order now');
		define('IMIE_NAZWISKO', 'First Name and Surname');
		define('TRESC', 'Text');
		define('ZAKONCZ', 'End');
		define('ZASTRZEZONE', 'Hidden');
		define('WROC', 'Back');
		define('WYSLIJ', 'Send');
		define('EDYTUJ', 'Edit');
		define('ZMIEN', 'Change');
		define('TUTAJ', 'here');
		define('USUN', 'Delete');
		define('DODAJ', 'Add');
		define('CENA', 'Price');
		define('TRESC', 'Text');
		define('PODSUMOWANIE', 'Summary');
		define('HASLO', 'Password');
		define('LOGUJ', 'Log In');
		define('BRAK_KONTA', 'No Account');
		define('NOWE_KONTO', 'New Account');
		define('MAIL_ZAJETY', 'Selected e-mail is already taken');
		define('PRZEGLADARKA', 'Browser');
		
		// form ver
		define('ZAKRES_IMIE', 'Improper number of characters  ( 3 to 55 characters)');
		define('ZAKRES_TRESC', 'Improper number of characters ( 3 to 500 characters)');
		define('ZAKRES_HASLO', 'Given password must have more than 5 characters');
		define('EMAIL_BLAD', 'Wrong email address');
		define('BLAD_LOGOWANIA', 'Login Error - Incorrect Data');
		define('KOSZYK_ILOSC_BLAD', 'Numbers in digits !');
		define('KOSZYK_ILOSC_ZADUZO', 'Number of items not available !');
		define('IMIE_PUSTE', 'Improper number of characters (3 to 55 characters)');
		define('IMIE_BLAD', 'Given name may include only letters and the character _ !');
		define('NAZWISKO_PUSTE', 'Improper number of characters ( 3 to 55 characters)');
		define('NAZWISKO_BLAD', 'Given surname may include only letters and the character _ !');
		define('NAZWA_FIRMY_BLAD', 'Company\'s name must have more than 3 characters');
		define('NIP_BLAD', 'Number NIP must have 10 digits, character - is not compulsory');
		define('PESEL_BLAD', 'Number PESEL must have 11 digits');
		define('ADRES_PUSTE', 'Not complete address');
		define('REGON_BLAD', 'Incorrect REGON number');
		define('KOD_BLAD', 'Incorrect zip code');
		define('MIASTO_PUSTE', 'Improper number of characters ( 3 to 145 characters)');
		define('MIASTO_BLAD', 'Given name of the city may include only letters and the character _ !');
		define('HASLA_BLAD', 'Given paswords differ from each other !');
		define('BLAD_DOM', 'Telephone number may include only digits !');
		define('BLAD_KOM', 'Mobile telephone number may include only digits !');
		define('BANK_BLAD', 'Improper number of characters ( 3 to 245 characters)');
		define('NR_KONTA_BLAD', 'Given Bank Account Number does not comply with NRB directive');
		define('TRESC_BLAD' ,'Add text ');
		define('KSIEGA_SUKCES', 'Text added !');
		define('KOM_RATE', 'Add comment and rate the product');
		define('OCEN_PRODUKT', 'Please evaluate the product!');
		
		// menu
		define('TWOJA_STRONA', 'Your page');
		define('OFERTA', 'Offer');
		define('BUKIET', 'Bouquet');
		define('KSIEGA_GOSCI', 'Visitor\'s Book');
		define('KOSZYK', 'Basket');
		define('TWOJA_STRONA', 'Your Site');
		define('LOGOWANIE', 'Login');
		define('EDYCJA_DANYCH', 'Edit Data');
		define('DODAWANIE_USERA', 'Create New Account');
		define('ZMIANA_HASLA', 'Change Password');
		define('KOSZYK_POKAZ', 'Show Basket');
		define('HISTORY_LOG', 'Login History');
		define('HISTORY_ZAM', 'Order History');
		define('INFO_TWOJA_STRONA', 'Welcome to Administration Panel ! You can edit your data here, change your account password, view your basket, and check your login and order history.');
		define('DODAJ_USERA_INFO', 'Welcome to ADD NEW USER section ! <br /> * - required fields');
		define('EDYCJA_DANYCH_INFO', 'Welcome to EDIT YOUR DATA section !');
		
		//pola form
		define('IMIE', 'First Name');
		define('NAZWISKO', 'Surname');
		define('PESEL', 'PESEL NUMBER');
		define('NAZWA_FIRMY', 'Company\'s Name');
		define('NIP', 'NIP NUMBER');
		define('REGON', 'REGON NUMBER');
		define('ULICA', 'Street');
		define('KOD_POCZ', 'Zip Code');
		define('MIASTO', 'City');
		define('EMAIL', 'Email');
		define('HASLO', 'Password');
		define('HASLO_POW', 'Repeat Password');
		define('TEL_KOM', 'Mobile Number');
		define('TEL_DOM', 'Telephone Number');
		define('NAZWA_BANKU', 'Bank');
		define('NR_KONTA', 'Bank Account Number');
		define('OF', 'Natural ');
		define('FIRMA', 'Business');
		define('REJESTRUJESZ_SIE_JAKO', 'Sign up as a');
		
		//twoja strona
		define('DATA_LOGOWANIA', 'Date of login');
		
		define('HASLO_ZMIENIONE', 'Password changed !');
		define('HASLO_BLAD', 'Old password does not match the entered');
		
		define('HASLO_VAL_OLD', 'The old password must be more than 5 characters');
		define('HASLO_NEW_CHARS', 'The new password must be more than 5 characters');
		define('HASLO_VAL_NEW', 'Newly given passwords are not the same !');
		
		define('HASLO_STARE', 'Old Password');
		define('HASLO_NEW', 'New Password');
		define('HASLO_NEW_REPLY', 'Repeat new password');
		
		define('NR_ZAMOWIENIA', 'No. of contract');
		define('SZCZEGOLY', '	details');
		define('Z_DNIA', 'of');
		define('O_GODZINIE', 'at');
		
		define('SZCZEGOLY_ORDER_TEXT', 'No. details of the contract');
		
		//tabelka
		define('NAZWA_PRODUKTU' ,'Product Name');
		define('ILOSC' ,'Amount');
		define('RAZEM' ,'Total');
		define('WSUMIE' ,'Total');
		define('ROZMIAR', 'Size');
		define('WYBIERZ_KURIERA' ,'Choose Shipping Company');
		define('KURIER_INFO' ,'Available shipping companies for 
this order, choose most suitable for you:');
		
		define('WERYFIKCJA', 'Verification');
		define('WERYFIKCJA_OK', 'This is an order you wish to place - check it and accept');
		define('WERYFIKCJA_WROC', 'should everything be correct or modify');
		
		define('ORDER_OK_MAIL', 'An email with similar content was sent to the address you have presented at the registration');
		
		define('WYLOGOWANIE_OK', 'Logout successful !');
		define('NIEMA_SWIAT', 'No Holiday Today');
		define('CURIER_ERROR_LOGIN', 'Error ! - To place your order need to be logged, if you do not have an account you can sign up for free');
		define('DATA_DODANIA', 'Date added');
		define('ZNALEZIONO_BLEDY', 'Errors Found !');
		define('USUNIETO', 'Deleted !');
		define('OTO_KUPIONY_TOWAR', '	Here are the goods bought');
		define('ORDER_DESC', 'If you have any please describe it below');
		define('BRAK_KURIERA', 'There is such a courier! Please choose again another courier');
		define('KOLOR', 'Color');
	}
}

?>
