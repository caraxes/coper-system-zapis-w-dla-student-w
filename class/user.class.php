<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
	class user {
	 
	  protected $host;
	  protected $user;
	  protected $pwd;
	  protected $dbName;
	 
		 function __construct($host, $user, $pwd, $dbName){
			$this->host = $host;
			$this->user = $user;
			$this->pwd = $pwd;
			$this->dbName = $dbName;
		}
		
		public function addStudent() {
			try 
			{
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			
				if($_SERVER['REQUEST_METHOD'] == 'POST') 
				{ 	
					try
				   {
									

					//DATA VERIFICATION:  
					  
					$formval = new formValidator();
					
					
						$formval -> validateEmpty('name',IMIE_PUSTE,3,55);
						$formval -> validateAlphabetic('name',IMIE_BLAD);
						$formval -> validateEmpty('surname',NAZWISKO_PUSTE,3,55);
						$formval -> validateAlphabetic('surname',NAZWISKO_BLAD);
						//$formval -> validateEmpty('company_name',NAZWA_FIRMY_BLAD,3,55);
						//$formval -> validateNIP('nip',NIP_BLAD); //'894-285-16-30'
						//$formval -> validatePESEL('pesel',PESEL_BLAD); //'87121406375'
						$formval -> validateEmpty('address',ADRES_PUSTE,3,300);
						//$formval -> validateREGON('regon',REGON_BLAD); // 590096454
						$formval -> validatZipNumber('address_zip_number',KOD_BLAD);
						$formval -> validateEmpty('city_name',MIASTO_PUSTE,3,145);
						$formval -> validateAlphabetic('city_name',MIASTO_BLAD);
						$formval -> validateEmail('email',EMAIL_BLAD);					
						$formval -> validateEmpty('password',ZAKRES_HASLO,5,145);
						//$formval -> validateCompare('password','password2',HASLA_BLAD);
						//$formval -> validateNumber('phone_number',BLAD_DOM);
						$formval -> validateNumber('mobile_number',BLAD_KOM);
						//$formval -> validateEmpty('bank_name',BANK_BLAD,3,245);
						//$formval -> validateNumerNRB($_POST['bank_account'],NR_KONTA_BLAD);
						
					
					
					if(isset($_POST['password'])) $password = md5($_POST['password']);	
						$bank_account = str_replace(' ', '', $_POST['bank_account']);
						$formval -> validateMail('email',$pdo,'wybrany email jest zajety');	
						
						$formval_errors_number = $formval -> checkErrors();
							if($formval_errors_number > 0)
								echo $formval -> displayErrors();
							
							//print_R($_POST);

							$threeYears = strtotime('+3 years', time());
							$index=rand(100, 100000);
							
					//die($_POST['rodzaj']);
					   $sql = $pdo -> prepare("INSERT INTO users (
								`name` ,
								`surname` ,
								`pesel` ,
								`address` ,
								`address_zip_number` ,
								`address_city_name` ,
								`email` ,
								`password` ,
								`mobile_number` ,
								`bank_name` ,
								`bank_account` ,
								`index` ,
								`dataSTART` ,
								`dataKONIEC` ,
								`id_specjalnosc` ,
								`status` ,
								`type` ,
								`date_add` ,
								`login_add`
								) VALUES (
								:name,
								:surname,
								:pesel,
								:address,
								:address_zip_number,
								:address_city_name,
								:email,
								:password,
								:mobile_number,
								:bank_name,
								:bank_account,
								".$index.",
								NOW(),
								'".date('Y-m-d H:m:s', $threeYears)."',
								:specjlanosc,
								1,
								3,
								NOW(),
								'rejestracja')");
								
					  $sql -> bindParam(':name', $_POST['name'], PDO::PARAM_STR, 85);
					  $sql -> bindParam(':surname', $_POST['surname'], PDO::PARAM_STR, 85);
					  $sql -> bindParam(':pesel', $_POST['pesel'], PDO::PARAM_STR, 11);
					  $sql -> bindParam(':address', $_POST['address'], PDO::PARAM_STR, 100);
					  $sql -> bindParam(':address_zip_number', $_POST['address_zip_number'], PDO::PARAM_STR, 6);
					  $sql -> bindParam(':address_city_name', $_POST['city_name'], PDO::PARAM_STR, 145);
					  $sql -> bindParam(':email', $_POST['email'], PDO::PARAM_STR, 145);
					  $sql -> bindParam(':password', $password, PDO::PARAM_STR, 55);
					  $sql -> bindParam(':mobile_number', $_POST['mobile_number'], PDO::PARAM_STR, 45);
					  $sql -> bindParam(':bank_name', $_POST['bank_name'], PDO::PARAM_STR, 245);
					  $sql -> bindParam(':bank_account', $bank_account, PDO::PARAM_STR, 26);
					  $sql -> bindParam(':specjlanosc', $_POST['specjalnosc'], PDO::PARAM_INT, 2);
					
				//print_R($sql); die();	
					   if($formval_errors_number == 0) {
							$sql -> execute();
							
							//print_R($sql->errorInfo());
							
							$sql->closeCursor();
							
							echo "Dodano nowego studenta !<br />";
							echo "Dane do logowania dla ".$_POST['name']." ".$_POST['surname'].":<br />";
							echo "Login to nr indeksu: ".$index." a hasło to ".$_POST['password'];
							
					   }
				   }
				   catch(PDOException $e)
				   {
					  echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
				   }
				}
			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function delStudent($id) {
			try 
			{
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				
				$sql = $pdo -> exec("UPDATE `users` SET status = 0 WHERE id_user = ".$id);
				   
			}	   
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		public function getAllStudents() {
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				$sql = $pdo->query("SELECT *, specjalnosc.nazwa as specjalnosc_nazwa, wydzial.nazwa as wydzial_nazwa FROM users 
				INNER JOIN specjalnosc ON users.id_specjalnosc = specjalnosc.id_specjalnosc
				INNER JOIN wydzial ON specjalnosc.id_wydzial = wydzial.id_wydzial
				WHERE users.status=1 AND users.type=3");
				$data = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $data;
				
		}
		
		public function getStudent($id) { 
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				$sql = $pdo->query("SELECT *, specjalnosc.nazwa as specjalnosc_nazwa, wydzial.nazwa as wydzial_nazwa FROM users 
				INNER JOIN specjalnosc ON users.id_specjalnosc = specjalnosc.id_specjalnosc
				INNER JOIN wydzial ON specjalnosc.id_wydzial = wydzial.id_wydzial
				WHERE users.status=1 AND users.type=3 AND users.id_user =".$id); 
				$data = $sql->fetch(PDO::FETCH_ASSOC);
				return $data;
				
		}
		
		public function getAllWykladowcy() {
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
				$sql = $pdo->query("SELECT *, specjalnosc.nazwa as specjalnosc_nazwa, wydzial.nazwa as wydzial_nazwa, stopien.nazwa as stopien_nazwa FROM users 
				INNER JOIN specjalnosc ON users.id_specjalnosc = specjalnosc.id_specjalnosc
				INNER JOIN wydzial ON specjalnosc.id_wydzial = wydzial.id_wydzial
				INNER JOIN stopien ON users.id_stopien 	= stopien.id_stopien
				WHERE users.status=1 AND users.type=2");
				$data = $sql->fetchAll(PDO::FETCH_ASSOC);
				return $data;
				
		}
		
		public function getSpecjalnosci() {
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			$sql = $pdo->query("SELECT * FROM specjalnosc");
			$data = $sql->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		
		}
		
		
		public function editStudent($id) {
		
		$user_data = array();
		
		if(!isset($_SESSION['user_id'])||!is_numeric($_SESSION['user_id']))
			die ("Błąd Krytyczny, brak odpowiednich danych, może nie jesteś zalogowany.");
			
			try 
			{
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'');
			
				
				
				if($_SERVER['REQUEST_METHOD'] == 'POST') 
				{ 	
					try
				   {
					
					//DATA VERIFICATION:  
					$formval = new formValidator();

						$formval -> validateEmpty('name',IMIE_PUSTE,3,55);
						$formval -> validateAlphabetic('name',IMIE_BLAD);
						$formval -> validateEmpty('surname',NAZWISKO_PUSTE,3,55);
						$formval -> validateAlphabetic('surname',NAZWISKO_BLAD);
						//if($user_data['type'] == '1') $formval -> validatePESEL('pesel',PESEL_BLAD); //'87121406375'
						
						$formval -> validateEmpty('address',ADRES_PUSTE,3,300);
						$formval -> validatZipNumber('address_zip_number',KOD_BLAD);
						$formval -> validateEmpty('city_name',MIASTO_PUSTE,3,145);
						$formval -> validateAlphabetic('city_name',MIASTO_BLAD);
						//$formval -> validateNumber('phone_number',BLAD_DOM);
						$formval -> validateNumber('mobile_number',BLAD_KOM);
						//$formval -> validateEmpty('bank_name',BANK_BLAD,3,245);
						//$formval -> validateNumerNRB($_POST['bank_account'],NR_KONTA_BLAD);
					
					$formval_errors_number = $formval -> checkErrors();
							if($formval_errors_number > 0)
								echo $formval -> displayErrors();
					//DATA VERIFICATION end: 
					
					  $sql = $pdo -> prepare("UPDATE `users` SET 
					  `name` = :name,
					  `surname` = :surname,
					 
					  `address` = :address,
					  `address_zip_number` = :address_zip_number,
					  `address_city_name` = :address_city_name,
					  
					  `mobile_number` =  :mobile_number,
					  `bank_name` = :bank_name,
					  `bank_account` = :bank_account,
					  `id_specjalnosc` = :spec
						WHERE id_user = '".$id."' ");

					  $sql -> bindParam(':name', $_POST['name'], PDO::PARAM_STR, 85);
					  $sql -> bindParam(':surname', $_POST['surname'], PDO::PARAM_STR, 85);
					
					  $sql -> bindValue(':address', $_POST['address'], PDO::PARAM_STR);
					  $sql -> bindParam(':address_zip_number', $_POST['address_zip_number'], PDO::PARAM_STR, 6);
					  $sql -> bindParam(':address_city_name', $_POST['city_name'], PDO::PARAM_STR, 145);
					 // $sql -> bindParam(':phone_number', $_POST['phone_number'], PDO::PARAM_STR, 19);
					  $sql -> bindParam(':mobile_number', $_POST['mobile_number'], PDO::PARAM_STR, 45);
					  $sql -> bindParam(':bank_name', $_POST['bank_name'], PDO::PARAM_STR, 245);
					  $sql -> bindParam(':bank_account', $_POST['bank_account'], PDO::PARAM_INT, 26);
					   $sql -> bindParam(':spec', $_POST['specjalnosc'], PDO::PARAM_INT, 26);
					 
					   if($formval_errors_number == 0) {
							$sql -> execute();
							//print_R($sql->errorInfo());
							$sql->closeCursor();
							
							echo "dane zmieniono !";
							
							
							echo "<script>setTimeout ( \"document.location = 'dziekanat_student_edytuj?id=".$id."'\",1000)</script>";
							
						}
					  
					  
				   }
				   catch(PDOException $e)
				   {
					  echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
				   }
				}
			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		}
		
		
		
	}
	 

	 
?>
