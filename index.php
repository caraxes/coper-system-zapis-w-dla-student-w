<?php
//session start
session_start();

	  //jezyk
	  $_SESSION['l'] = '1';
	  //zaladowanie pliku lang z definowanymi nazwami dla jezyków
	  include('class/langs.php');
	  
$_SESSION['_email'] = 'biuro@mail.com';
$_SESSION['_email_name'] = 'NADAWCA';

if(!isset($_SESSION['user_type'])) $_SESSION['user_type'] = '66';


header ('Content-type: text/html; charset=utf-8');
//print_R($_SESSION);

// set session life time
ini_set('session.gc_maxlifetime', '300'); // 300 sec = 5 min


error_reporting(E_ALL);
ini_set('display_errors', '1');
//autoload user-defined classes
      function __autoload($class){
          include 'class/'.$class.'.class.php';
      }

	define('H_O_ST', "localhost");
	define('U_S_ER', "user");
	define('P_A_SS', "pass");
	define('D_B_AS', "COPER");

      



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>COPER System Zapisów dla Studentów</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/menu.css" />
</head>
<body>


<!-- poczatek header -->
<h1 style="font-size:30px;">COPER System Zapisów dla Studentów</h1>
<!-- koniec menu -->


<?php /*
if($_GET['goto']!='start') echo '<div class="menu">
<div class="navigacja">
	<ul id="nav">
		<li class="top"><a href="start" class="top_link"><span class="down">strona główna</span></a></li>
		<li class="top"><a href="oferta_pracodawca" class="top_link"><span class="down">oferta dla pracodawcy</span></a></li>
        <li class="top"><a href="oferta_pracownik" class="top_link"><span class="down">oferta dla pracownika</span></a></li>
        <li class="top"><a href="onas" class="top_link"><span class="down">o nas</span></a></li>
        <li class="top"><a href="kontakt" class="top_link"><span class="down">kontakt</span></a></li>
        <li class="top"><a href="zaufalinam" class="top_link"><span class="down">zaufali nam</span></a></li>
   	</ul>
</div>
</div>';
*/

	if(isset($_SESSION['user_id'])&&isset($_SESSION['user_type'])) {
		$user = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
		$usdata = $user->getUserData($_SESSION['user_id']);
		
		if($usdata['type']=='1') {
		echo '<div class="menu">
			<div class="navigacja">
				<ul id="nav">
					<li class="top"><a href="dziekanat" class="top_link"><span class="down">Home</span></a></li>
					<li class="top"><a href="dziekanat_student" class="top_link"><span class="down">Zarządzaj studentami</span></a></li>
					<li class="top"><a href="dziekanat_kursy" class="top_link"><span class="down">Zarządzaj kursami</span></a></li>
			        <li class="top"><a href="dziekanat_oplaty" class="top_link"><span class="down">Zarządzaj opłatami</span></a></li>
			        <li class="top"><a href="dziekanat_zapisy_daty" class="top_link"><span class="down">Daty zapisów</span></a></li>
			   	</ul>
			</div>
			</div>';
		}elseif($usdata['type']=='2') {
		echo '<div class="menu">
			<div class="navigacja">
				<ul id="nav">
					<li class="top"><a href="wykladowca" class="top_link"><span class="down">Home</span></a></li>
					<li class="top"><a href="wykladowca_lista_zajec" class="top_link"><span class="down">Lista zajęć</span></a></li>
					</ul>
			</div>
			</div>';
		}elseif($usdata['type']=='3') {
		echo '<div class="menu">
			<div class="navigacja">
				<ul id="nav">
					<li class="top"><a href="student" class="top_link"><span class="down">Home</span></a></li>
					<li class="top"><a href="student_zapisy_lista" class="top_link"><span class="down">Zapisy na kursy</span></a></li>
					<li class="top"><a href="student_zapisy_moje" class="top_link"><span class="down">Moje kursy</span></a></li>
					<li class="top"><a href="student_oplaty" class="top_link"><span class="down">Moje opłaty</span></a></li>
				</ul>
			</div>
			</div>';
		}
	}
	
?>

<!-- poczatek content -->
<div class="content">
	

	
    <?php

	if(isset($_SESSION['user_id'])) {
						$login = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
					$user = $login -> getUserData($_SESSION['user_id']);
					echo '
					<div style="margin-top:10px; text-align:right;">
					Jesteś zalogowany jako:  '.$user['name'].' '.$user['surname'].'
					  <a href="logout"><img src="images/wyloguj.jpg" alt="wyloguj"></a></div>';
					}


switch ($_GET['goto']) {
		case 'logout':
			
		$login = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
		$login -> logout();
		echo "<h1>Wylogowanie zakończone sukcesem</h1>";
		echo "Dziekujemy i zapraszamy ponownie";
		echo "<script>setTimeout ( \"document.location = 'start'\",1000)</script>";
		break;
		case 'kontakt':
		echo '<div class="content_t"><h1>Kontakt</h1></div>
    		<div class="content_c">
   		<div class="dane">
		   <div class="left_contact">dane kontaktowe</div>
		<form method="post" action="kontakt"><div class="right_contact">';
		$mail = new mail();
		$mail->send();
		$vars = new vars();
		echo '
		   
		   <p class="contact_p"><label class="contact_label">Imię i Nazwisko:</label><input name="imie" value="'.$vars->show('imie').'" class="contact_input" /></p>
		   <p class="contact_p"><label class="contact_label">E-mail:</label><input name="email" value="'.$vars->show('email').'" class="contact_input" /></p>
		  	<p class="contact_p"><label class="contact_label">Treść:</label><textarea name="comment" class="contact_text" cols="30" rows="15" name="comment3">'.$vars->show('comment').'</textarea></p>
		   <p class="contact_p"><input class="contact_button" value="wyślij zapytanie" type="submit" /></p>
		   </div>
		   </form>
		   <div class="clear_contact"></div>
		   
		 ';
		
			
		break;
		case 'start':
		echo '<div class="content_t"><h1>Witamy na stronie Systemu Zapisów COPER</h1></div>
    		<div class="content_c">
   		<div class="dane">
        <div class="kat">
    		<div class="kat_left">
            	<div class="kat_t"><div class="kat_title">Zaloguj sie na konto:</div></div>
                <div class="kat_c">
                	 	<div><input class="kat_button" value="Studenta" onclick="parent.location=\'login_st\'" type="submit" /></div>  
                	 	 	<div><input class="kat_button" value="Pracownika dziekanatu" onclick="parent.location=\'login_pd\'" type="submit" /></div>  
 <div class="kat_b">
                	<div><input class="kat_button" value="Wykładowcy" onclick="parent.location=\'login_wy\'" type="submit" /></div>                
                	
                </div>
                </div>
               
            </div>
        	
        	<div class="kat_clear"></div>
        </div>';
		
		break;
		case 'login_st':
		echo '<div class="content_t"><h1>Logowanie na konto STUDENTA</h1></div>
    		<div class="content_c">
   		<div class="dane">
        <div class="kat">
    		<div class="kat_left">
            	<div class="kat_t"><div class="kat_title">Podaj dane:</div></div>
                <div class="kat_c">
                <form method="post" action="login_st">
                	 	<label class="registration_label">Numer indeksu</label><input name="login" value="" class="registration_text" /> 189342
          <label class="registration_label">Hasło:</label><input name="password" value="" class="registration_text" /> test123
          
          <p class="registration_p"><input class="registration_button" value="Zaloguj mnie !" type="submit" /></p>
          
   		</form>
          
 <div class="kat_b">';
		$login = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
		$login -> authorization(3);
             echo '</div>
                </div>
            </div>
        	<div class="kat_clear"></div>
        </div>';
		
		break;
		case 'login_pd':
		echo '<div class="content_t"><h1>Logowanie na konto Pracownika dziekanatu</h1></div>
    		<div class="content_c">
   		<div class="dane">
        <div class="kat">
    		<div class="kat_left">
            	<div class="kat_t"><div class="kat_title">Podaj dane:</div></div>
                <div class="kat_c">
                <form method="post" action="login_pd">
                	 	<label class="registration_label">Numer pracownika</label><input name="login" value="" class="registration_text" /> 1234
          <label class="registration_label">Hasło:</label><input name="password" value="" class="registration_text" /> test123
          
          <p class="registration_p"><input class="registration_button" value="Zaloguj mnie !" type="submit" /></p>
          
   		</form>
          
 <div class="kat_b">';
		$login = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
		$login -> authorization(1);
             echo '</div>
                </div>
            </div>
        	<div class="kat_clear"></div>
        </div>';
             break;
        case 'login_wy':
        			
		echo '<div class="content_t"><h1>Logowanie na konto Wykładowcy</h1></div>
    		<div class="content_c">
   		<div class="dane">
        <div class="kat">
    		<div class="kat_left">
            	<div class="kat_t"><div class="kat_title">Podaj dane:</div></div>
                <div class="kat_c">
                <form method="post" action="login_wy">
                	 	<label class="registration_label">Numer pracownika</label><input name="login" value="" class="registration_text" /> XXX006
          <label class="registration_label">Hasło:</label><input name="password" value="" class="registration_text" /> test123
          
          <p class="registration_p"><input class="registration_button" value="Zaloguj mnie !" type="submit" /></p>
          
   		</form>
          
 <div class="kat_b">';
		$login = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
		$login -> authorization(2);
             echo '</div>
                </div>
            </div>
        	<div class="kat_clear"></div>
        </div>';
		
		break;
        case 'dziekanat':
        	//print_R($_SESSION); die();
        	if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
        		echo "<script>document.location = 'start'</script>";
        	} else { 
        		echo '<div class="content_t"><h1>Dziekanat COPER wita, co chcesz dziś zrobić ?</h1></div>
        		    		<div class="content_c">
        		   		<div class="dane">';
        		$user_data = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
        		
        		$user = $user_data->getUserData($_SESSION['user_id']);
        		echo "<h2>WITAJ ".$user['name']." ".$user['surname']."</h2>";
        		
        		
        		$com = new comments(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
        		
        		$data = $com->getComments();
        		
        		$com->editComments();
        		$vars = new vars();
        		echo "<h3>Aktualności - dodaj </h3>";
        		echo '<form action="dziekanat" method="post">
        		       									<p class="registration_p"><label class="registration_label">*OPIS:</label><textarea style="width:400px; height:100px;" name="opis">'.$vars->show('do').'</textarea></p>
        		       									<p class="registration_p"><input class="registration_button" value="Dodaj" type="submit" /></p>
        		       								</form>';
        		
        		foreach($data as $d) {
        			echo "<p><u>".$d['created_at']."</u> <strong>".$d['name']." ".$d['surname']."</strong> - ".$d['description']."</p>";
        		}
        	}
      	
        break;
        case 'dziekanat_student':
        	if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
        		echo "<script>document.location = 'start'</script>";
        	} else {
        		echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie studentami</h1></div>
                		    		<div class="content_c">
                		   		<div class="dane">';
        		$students = new user(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
        		$sdata = $students->getAllStudents();
        		//print_r($sdata);
        		
        		echo '<a href="dziekanat_student_dodaj">Dodaj nowego studenta</a>';
        		
        		echo "<table>
        		<tr>
				    <th>Lp.</th>
				    <th>Nr. Indexu</th>
				    <th>Specjalność</th>
				    <th>Wydział</th>
				    <th>Imie i Nazwisko</th>
				    <th>Opcje</th>
				  </tr>";
        		$i=1;
				foreach($sdata as $sd){ 
        			echo "
				  <tr>
				    <td>".$i."</td>
				    <td>".$sd['index']."</td>
				    <td>".$sd['specjalnosc_nazwa']."</td>
				    <td>".$sd['wydzial_nazwa']."</td>
				    <td>".$sd['name']." ".$sd['surname']."</td>
				    <td><a href='dziekanat_student_edytuj?id=".$sd['id_user']."'>EDYTUJ</a> <a href='dziekanat_student_usun?id=".$sd['id_user']."' onclick='return confirm('Chcesz usunąć tego studenta ??')'>USUŃ</a></td>
				  </tr>";
        			$i++;
				}
        		echo "</table>";
        	}
        	 
        	break;
        	case 'dziekanat_student_dodaj':
        		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
        			echo "<script>document.location = 'start'</script>";
        		} else {
        			$vars = new vars();
        			echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie studentami - dodawanie nowego studenta</h1></div>
        	                		    		<div class="content_c">
        	                		   		<div class="dane">';
        			
        			$student = new user(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
        			$spec = $student->getSpecjalnosci();
        			
        			$student->addStudent();
        			
        			echo '<form action="dziekanat_student_dodaj" method="post">
							<p class="registration_p"><label class="registration_label">*Specjalność:</label>
							
							<select name="specjalnosc">';
        			foreach ($spec as $s) echo '<option value="'.$s['id_specjalnosc'].'">'.$s['nazwa'].'</option>';
							echo '</select> 
							</p>
							<p class="registration_p"><label class="registration_label">*Imię:</label><input name="name" type="text" value="'.$vars->show('name').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Nazwisko:</label><input name="surname" type="text" value="'.$vars->show('surname').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Pesel:</label><input name="pesel" type="text" value="'.$vars->show('pesel').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Ulica:</label><input name="address" type="text" value="'.$vars->show('address').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Kod pocztowy:</label><input name="address_zip_number" type="text" value="'.$vars->show('address_zip_number').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Miasto:</label><input name="city_name" type="text" value="'.$vars->show('city_name').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Email:</label><input name="email" type="text" value="'.$vars->show('email').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Hasło:</label><input name="password" type="password" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Telefon kontaktowy:</label><input name="mobile_number" type="text" value="'.$vars->show('mobile_number').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">Nazwa banku:</label><input name="bank_name" type="text" value="'.$vars->show('bank_name').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">Numer konta:</label><input name="bank_account" type="text" value="'.$vars->show('bank_account').'" class="registration_text" /></p>
							<p class="registration_p"><input class="registration_button" value="dodaj" type="submit" /></p>
						</form>';
        		}
       		 break;
	        		case 'dziekanat_student_edytuj':
	        		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
	        			echo "<script>document.location = 'start'</script>";
	        		} else {
        			$vars = new vars();
        			echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie studentami - edycja studenta</h1></div>
        	                		    		<div class="content_c">
        	                		   		<div class="dane">';
        			if($vars->get('id')) {
	        			$student = new user(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
	        			$spec = $student->getSpecjalnosci();
	        			
	        			$data = $student->getStudent($vars->show('id'));
	        			
	        			$student->editStudent($vars->show('id'));
	        			
	        			echo '<form action="dziekanat_student_edytuj?id='.$vars->show('id').'" method="post">
								<p class="registration_p"><label class="registration_label">*Specjalność:</label>
							
								<select name="specjalnosc">';
	        			foreach ($spec as $s) { if($s['id_specjalnosc']==$data['id_specjalnosc']) $now = " selected='selected' "; else $now =""; echo '<option value="'.$s['id_specjalnosc'].'" '.$now.'>'.$s['nazwa'].'</option>';}
							echo '</select> 
							</p>
							<p class="registration_p"><label class="registration_label">*Imię:</label><input name="name" type="text" value="'.$data['name'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Nazwisko:</label><input name="surname" type="text" value="'.$data['surname'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Pesel:</label><input name="pesel" type="text" value="'.$data['pesel'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Ulica:</label><input name="address" type="text" value="'.$data['address'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Kod pocztowy:</label><input name="address_zip_number" type="text" value="'.$data['address_zip_number'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Miasto:</label><input name="city_name" type="text" value="'.$data['address_city_name'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Email:</label><input name="email" type="text" value="'.$data['email'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Telefon kontaktowy:</label><input name="mobile_number" type="text" value="'.$data['mobile_number'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">Nazwa banku:</label><input name="bank_name" type="text" value="'.$data['bank_name'].'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">Numer konta:</label><input name="bank_account" type="text" value="'.$data['bank_account'].'" class="registration_text" /></p>
							<p class="registration_p"><input class="registration_button" value="edytuj" type="submit" /></p>
						</form>';
				}
        		}
       		 break;
        		case 'dziekanat_student_usun':
        		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
        			echo "<script>document.location = 'start'</script>";
        		} else {
        			$vars = new vars();
		echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie studentami - usuwanie studenta</h1></div>
        	                		    		<div class="content_c">
        	                		   		<div class="dane">';
		if($vars->get('id')) {
			
        			$student = new user(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
        			$del = $student->delStudent($vars->show('id'));
        			echo 'USUNIETO ! <a href="dziekanat_student">wróć</a>';
        		} else echo 'brak danych';
        		}	
        break;
        
        
        case 'dziekanat_kursy':
        	if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
        		echo "<script>document.location = 'start'</script>";
        	} else {
        		echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie kursami</h1></div>
                        		    		<div class="content_c">
                        		   		<div class="dane">';
        		$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
        		$sdata = $kurs->getAllKursy();
        		//print_r($sdata);
        
        		echo '<a href="dziekanat_kursy_dodaj">Dodaj nowy kurs</a>';
        
        		echo "<table>
                		<tr>
        				    <th>Lp.</th>
        				    <th>Kod kursu</th>
        				    <th>Kod grupy</th>
        				    <th>Nazwa kursu</th>
        				    <th>Prowadzacy</th>
        				    <th>ECTS</th>
        				    <th>Forma zaliczenia</th>
        				    <th>Miejsca</th>
        				    <th>Płatny</th>
        				    <th>Opcje</th>
        				  </tr>";
        		$i=1;
        		foreach($sdata as $sd){
        			echo "
        				  <tr>
        				    <td>".$i."</td>
        				    <td>".$sd['kod_kursu']."</td>
        				    <td>".$sd['kod_grupy']."</td>
        				    <td>".$sd['nazwa']."</td>
        				    <td>".$sd['name']." ".$sd['surname']."</td>
        				    <td>".$sd['ects']."</td>";
        			if($sd['forma_zaliczenia']=='1') $fz = 'Egzamin';
        			else $fz = 'Zaliczenie';
        				    echo "<td>".$fz."</td>";
        				  
        				  echo "<td>".$sd['miejsca']."</td>";
					
        		 	if($sd['platny']=='1') $platny = 'TAK - '.$sd['ile_platny'].' PLN';
        			 else $platny = 'NIE';
        				  echo "<td>".$platny."</td>";
        				  
        				 echo"<td><a href='dziekanat_kursy_edytuj?id=".$sd['id_kurs']."'>EDYTUJ</a> <a href='dziekanat_kursy_usun?id=".$sd['id_kurs']."' onclick='return confirm('Chcesz usunąć ten kurs ??')'>USUŃ</a></td>
        				  </tr>";
        			$i++;
        		}
        		echo "</table>";
        	}
        
        	break;
       		 case 'dziekanat_kursy_dodaj':
        		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
        			echo "<script>document.location = 'start'</script>";
        		} else {
        			$vars = new vars();
        			echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie kursami - dodawanie nowego kursu</h1></div>
        	                		    		<div class="content_c">
        	                		   		<div class="dane">';
        			
        			$user = new user(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
        			$prowadzacy = $user->getAllWykladowcy();
        		//	print_r($prowadzacy);
        			$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
        			$kurs->addKurs();
        			
        			echo '<form action="dziekanat_kursy_dodaj" method="post">
							<p class="registration_p"><label class="registration_label">*Wykładowca:</label>
							
							<select name="prowadzacy">';
        			foreach ($prowadzacy as $p) echo '<option value="'.$p['id_user'].'">'.$p['stopien_nazwa'].' '.$p['name'].' '.$p['surname'].' - '.$p['wydzial_nazwa'].'</option>';
							echo '</select> 
							</p>
							<p class="registration_p"><label class="registration_label">*Nazwa:</label><input name="nazwa" type="text" value="'.$vars->show('nazwa').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Opis:</label><textarea name="opis">'.$vars->show('surname').'</textarea></p>
							<p class="registration_p"><label class="registration_label">*Ilość ECTS:</label>
							
							<select name="ects">';
        			for($i=1;$i<10;$i++) echo '<option value="'.$i.'">'.$i.'</option>';
							echo '</select> 
							</p>
							<p class="registration_p"><label class="registration_label">*Forma zaliczenia:</label>
							
							<select name="forma_zaliczenia">';
        							echo '<option value="1">Egzamin</option>';
        							echo '<option value="2">Zaliczenie</option>';
							echo '</select> 
							</p>
							<p class="registration_p"><label class="registration_label">*Kod kursu:</label><input name="kod_kursu" type="text" value="'.$vars->show('kod_kursu').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Kod grupy:</label><input name="kod_grupy" type="text" value="'.$vars->show('kod_grupy').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Termin od do:</label><input name="termin" type="text" value="'.$vars->show('termin').'" class="registration_text" /></p>
							<p class="registration_p"><label class="registration_label">*Ilość wolnych miejsc:</label>
							
							<select name="miejsca">';
        			for($i=1;$i<30;$i++) echo '<option value="'.$i.'">'.$i.'</option>';
							echo '</select> 
							</p>
							<p class="registration_p"><label class="registration_label">*Ilość wolnych miejsc minimum:</label>
							
							<select name="miejsca_minimum">';
        			for($i=1;$i<10;$i++) echo '<option value="'.$i.'">'.$i.'</option>';
							echo '</select> 
							</p>
							<p class="registration_p" style="color:#000;"><label class="registration_label">*Płatny ?</label>
							<input type="radio" name="platny" value="0"> NIE 
							<input type="radio" name="platny" value="1"> TAK
							</p>
							<p class="registration_p"><label class="registration_label">*Ile płatne (kwota w PLN):</label><input name="ile_platny" type="text" value="'.$vars->show('ile_platny').'" class="registration_text" /></p>
							<p class="registration_p"><input class="registration_button" value="dodaj" type="submit" /></p>
						</form>';
        		}
       		 break;
       		 break;
       		 case 'dziekanat_kursy_edytuj':
       		 	if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
       		 		echo "<script>document.location = 'start'</script>";
       		 	} else {
       		 		$vars = new vars();
       		 		if($vars->get('id')) {
	       		 		$vars = new vars();
	       		 		echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie kursami - edytowanie kursu</h1></div>
	       		         	                		    		<div class="content_c">
	       		         	                		   		<div class="dane">';
	       		 		 
	       		 		$user = new user(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
	       		 		$prowadzacy = $user->getAllWykladowcy();
	       		 		//	print_r($prowadzacy);
	       		 		
	       		 		
	       		 		
	       		 		$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
	       		 		
	       		 		$data = $kurs->getKurs($vars->show('id'));
	       		 		//print_r($data);
	       		 		$kurs->editKurs($vars->show('id'));
	       		 		 
	       		 		echo '<form action="dziekanat_kursy_edytuj?id='.$vars->show('id').'"" method="post">
	       		 							<p class="registration_p"><label class="registration_label">*Wykładowca:</label>
	       		 							
	       		 							<select name="prowadzacy">';
	       		 		foreach ($prowadzacy as $p) {
	       		 			if($p['id_user'] == $data['id_prowadzacy']) $act = 'selected="selected"';
	       		 			else $act = '';
	       		 			echo '<option value="'.$p['id_user'].'" '.$act.' >'.$p['stopien_nazwa'].' '.$p['name'].' '.$p['surname'].' - '.$p['wydzial_nazwa'].'</option>';
	       		 		}
	       		 		echo '</select>
	       		 							</p>
	       		 							<p class="registration_p"><label class="registration_label">*Nazwa:</label><input name="nazwa" type="text" value="'.$data['nazwa'].'" class="registration_text" /></p>
	       		 							<p class="registration_p"><label class="registration_label">*Opis:</label><textarea name="opis">'.$data['opis'].'</textarea></p>
	       		 							<p class="registration_p"><label class="registration_label">*Ilość ECTS:</label>
	       		 							
	       		 							<select name="ects">';
	       		 		for($i=1;$i<10;$i++){
	       		 			if($i == $data['ects']) $act = 'selected="selected"';
	       		 			else $act = '';
	       		 			echo '<option value="'.$i.'" '.$act.'>'.$i.'</option>';
	       		 		}
	       		 		echo '</select>
	       		 							</p>
	       		 							<p class="registration_p"><label class="registration_label">*Forma zaliczenia:</label>
	       		 							
	       		 							<select name="forma_zaliczenia">';
	       		 		if($data['forma_zaliczenia']=='1') { $act1 = 'selected="selected"'; $act2 = ''; }
	       		 		else { $act2 = 'selected="selected"'; $act1 = ''; }
	       		 		echo '<option value="1" '.$act1.'>Egzamin</option>';
	       		 		echo '<option value="2" '.$act2.'>Zaliczenie</option>';
	       		 		echo '</select>
	       		 							</p>
	       		 							<p class="registration_p"><label class="registration_label">*Kod kursu:</label><input name="kod_kursu" type="text" value="'.$data['kod_kursu'].'" class="registration_text" /></p>
	       		 							<p class="registration_p"><label class="registration_label">*Kod grupy:</label><input name="kod_grupy" type="text" value="'.$data['kod_grupy'].'" class="registration_text" /></p>
	       		 							<p class="registration_p"><label class="registration_label">*Termin od do:</label><input name="termin" type="text" value="'.$data['termin'].'" class="registration_text" /></p>
	       		 							<p class="registration_p"><label class="registration_label">*Ilość wolnych miejsc:</label>
	       		 							
	       		 							<select name="miejsca">';
	       		 		for($i=1;$i<30;$i++) {
	       		 			if($i == $data['miejsca']) $act = 'selected="selected"';
	       		 			else $act = '';
	       		 			echo '<option value="'.$i.'" '.$act.'>'.$i.'</option>';
	       		 		}
	       		 		echo '</select>
	       		 							</p>
	       		 							<p class="registration_p"><label class="registration_label">*Ilość wolnych miejsc minimum:</label>
	       		 							
	       		 							<select name="miejsca_minimum">';
	       		 		for($i=1;$i<10;$i++) { 
	       		 			if($i == $data['miejsca_minimum']) $act = 'selected="selected"';
	       		 			else $act = '';
	       		 			echo '<option value="'.$i.'" '.$act.'>'.$i.'</option>';
	       		 		}
	       		 		
	       		 		if($data['platny']=='0') {
	       		 			$act1 = 'checked="checked"'; $act2 = '';
	       		 		}
	       		 		else { $act2 = 'checked="checked"'; $act1 = '';
	       		 		}
	       		 		
	       		 		echo '</select>
       		 							</p>
       		 							<p class="registration_p" style="color:#000;"><label class="registration_label">*Płatny ?</label>
       		 							<input type="radio" name="platny" value="0" '.$act1.'> NIE 
       		 							<input type="radio" name="platny" value="1" '.$act2.'> TAK
       		 							</p>
       		 							<p class="registration_p"><label class="registration_label">*Ile płatne (kwota w PLN):</label><input name="ile_platny" type="text" value="'.$data['ile_platny'].'" class="registration_text" /></p>
       		 							<p class="registration_p"><input class="registration_button" value="edytuj" type="submit" /></p>
       		 						</form>';
       		 		}
       		 	}
       		 	break;
       		 case 'dziekanat_kursy_usun':
       		 	if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
       		 		echo "<script>document.location = 'start'</script>";
       		 	} else {
       		 		$vars = new vars();
       		 		echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie kursami - usuwanie kursu</h1></div>
       		         	                		    		<div class="content_c">
       		         	                		   		<div class="dane">';
       		 		if($vars->get('id')) {
       		 				
       		 			$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
       		 			$del = $kurs->delKurs($vars->show('id'));
       		 			echo 'USUNIETO ! <a href="dziekanat_kursy">wróć</a>';
       		 		} else echo 'brak danych';
       		 	}
       		 	break;
       		 	case 'dziekanat_oplaty':
       		 		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
       		 			echo "<script>document.location = 'start'</script>";
       		 		} else {
       		 			echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie oplatami</h1></div>
       		 	                        		    		<div class="content_c">
       		 	                        		   		<div class="dane">';
       		 			$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
       		 			$sdata = $kurs->getAllOplaty();
       		 			//print_r($sdata); die();
       		 	
       		 			
       		 			echo "<table>
       		 	                		<tr>
       		 	        				    <th>Lp.</th>
       		 	        				    <th>Od kogo</th>
       		 	        				    <th>Przedmiot/Kod</th>
       		 	        				    <th>Kwota</th>
       		 	        				   <th>Status</th>
       		 	        				    <th>Opcje</th>
       		 	        				  </tr>";
       		 			$i=1;
       		 			foreach($sdata as $sd){
       		 				echo "
       		 	        				  <tr>
       		 	        				    <td>".$i."</td>
       		 	        				    <td>".$sd['name']." ".$sd['surname']."</td>
       		 	        				    <td>".$sd['nazwa']." / ".$sd['kod_kursu']."</td>
       		 	        				    <td>".$sd['ile_platny']." PLN</td>";

       		 				if($sd['id_status_oplat']=='1') $stat = 'Zatwierdzony';
       		 				elseif($sd['id_status_oplat']=='2') $stat = 'Odrzucony';
       		 				else $stat = 'Nowy';
       		 					echo "<td>".$stat."</td>";
       		 				echo"<td>";
       		 				if($sd['id_status_oplat']=='0') {
       		 					
       		 					$id_zapis = $kurs->getIdZapis($sd['id_kurs'], $sd['id_user']);
       		 			
       		 					echo "<a href='dziekanat_oplaty_do?id=".$sd['id_oplata']."&amp;status=1&amp;zapis=".$id_zapis['id_zapis']."'>Zatwierdz</a> <a href='dziekanat_oplaty_do?id=".$sd['id_oplata']."&amp;status=2&amp;zapis=".$id_zapis['id_zapis']."'>Odrzuć</a>";
       		 				}
       		 				echo "</td>
       		 	        				  </tr>";
       		 				$i++;
       		 			}
       		 			echo "</table>";
       		 		}
       		 	
       		 break;
       		 case 'dziekanat_oplaty_do':
       		 	if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
       		 		echo "<script>document.location = 'start'</script>";
       		 	} else {
       		 		$vars = new vars();
       		 		echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie opłatami - zmiana statusu</h1></div>
       		        		         	                		    		<div class="content_c">
       		        		         	                		   		<div class="dane">';
       		 		if($vars->get('id') AND $vars->get('status') AND $vars->get('zapis')) {
       		 
       		 			$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
       		 			$del = $kurs->zmienStatusOplaty($vars->show('id'), $vars->show('status'), $vars->show('zapis'));
       		 			echo 'Zmieniono status ! <a href="dziekanat_oplaty">wróć</a>';
       		 		} else echo 'brak danych';
       		 	}
       		break;
       		case 'dziekanat_zapisy_daty':
       			if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='1') {
       				echo "<script>document.location = 'start'</script>";
       			} else {
       				$vars = new vars();
       				echo '<div class="content_t"><h1>Dziekanat COPER - Zarządzanie datami</h1></div>
       		        	                		    		<div class="content_c">
       		        	                		   		<div class="dane">';
       				 

       				$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
       				 
       				$data = $kurs->getDaty();
       				//print_r($data);
       				$kurs->editDaty();
       				 
       				echo '<form action="dziekanat_zapisy_daty" method="post">
       									<p class="registration_p"><label class="registration_label">*Data rozpoczęcia zapisów:</label><input name="od" type="text" value="'.$data['od'].'" class="registration_text" /></p>
       									<p class="registration_p"><label class="registration_label">*Data zakończenia zapisów:</label><input name="do" type="text" value="'.$data['do'].'" class="registration_text" /></p>
       									<p class="registration_p"><input class="registration_button" value="zmien" type="submit" /></p>
       								</form>';
       			}
       			break;
       		 	
////////////////////////////////////////////////////////////////////////////////////////////////////
       		 	
       		 	
       		 	case 'student':
       		 		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='3') {
       		 			echo "<script>document.location = 'start'</script>";
       		 		} else {
       		 			echo '<div class="content_t"><h1>Dziekanat COPER wita STUDENTA, co chcesz dziś zrobić ?</h1></div>
       		 	        		    		<div class="content_c">
       		 	        		   		<div class="dane">';
       		 			$user_data = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);

       		 			$user = $user_data->getUserData($_SESSION['user_id']);
       		 			echo "<h2>WITAJ ".$user['name']." ".$user['surname']."</h2>";
       		 			
       		 			
       		 			$com = new comments(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
       		 			
       		 			$data = $com->getComments();
     
  
       		 			echo "<h3>Aktualności </h3>";
       		 			
       		 			foreach($data as $d) {
       		 				echo "<p><u>".$d['created_at']."</u> <strong>".$d['name']." ".$d['surname']."</strong> - ".$d['description']."</p>";
       		 			}
       		 	
       		 		}
       		 		 
       		 break;
       		 
       		 case 'student_zapisy_moje':
       		 	if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='3') {
       		 		echo "<script>document.location = 'start'</script>";
       		 	} else {
       		 		echo '<div class="content_t"><h1>Dziekanat COPER - Przeglądanie kursów, na które się zapisałem</h1></div>
       		        		 		       		 	        		    		<div class="content_c">
       		        		 		       		 	        		   		<div class="dane">';
       		 		$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
       		 		$sdata = $kurs->getAllZapisaneKursy($_SESSION['user_id']);
       		 		//print_r($sdata);
       		 		$daty = $kurs->getDaty();
       		 
       		 		echo "<table>
       		        		 				                		<tr>
       		        		 				        				    <th>Lp.</th>
       		        		 				        				    <th>Kod grupy</th>
       		        		 				        				    <th>Nazwa kursu</th>
       		        		 				        				    <th>Prowadzacy</th>
       		        		 				        				    <th>ECTS</th>
       		        		 				        				    <th>Forma zaliczenia</th>
       		        		 				        				    <th>Płatny</th>
       		        		 				        				    <th>Status</th>
       		        		 				        				    <th>Opcje</th>
       		        		 				        				  </tr>";
       		 		$i=1;
       		 		foreach($sdata as $sd){
       		 			echo "
       		        		 				        				  <tr>
       		        		 				        				    <td>".$i."</td>
       		        		 				        				   
       		        		 				        				    <td>".$sd['kod_grupy']."</td>
       		        		 				        				    <td>".$sd['nazwa']."</td>
       		        		 				        				    <td>".$sd['name']." ".$sd['surname']."</td>
       		        		 				        				    <td>".$sd['ects']."</td>";
       		 			if($sd['forma_zaliczenia']=='1') $fz = 'Egzamin';
       		 			else $fz = 'Zaliczenie';
       		 			echo "<td>".$fz."</td>";
       		 
       		 			 
       		 			if($sd['platny']=='1') $platny = 'TAK';
       		 			else $platny = 'NIE';
       		 			echo "<td>".$platny."</td>";
       		 			
       		 			if($sd['status_zapisu']=='1') $stat = 'Zapisany';
       		 			elseif($sd['status_zapisu']=='2') $stat = 'Czekamy na zapłatę '.$sd['ile_platny'].' PLN';
       		 			elseif($sd['status_zapisu']=='3') $stat = 'Oczekiwanie na zatwierdzenie';
       		 			else $stat = 'Odrzucono - nie zapisany';
       		 			echo "<td>".$stat."</td>";
       		 			
       		 			if($daty['do'] > date("Y-m-d H:i:s")) echo"<td><a href='student_zapisy_wypisz?id=".$sd['id_zapis']."'>WYPISZ</a> ";
       		 			
       		 			if($sd['status_zapisu']=='2') echo "<a href='student_zaplac?id=".$sd['id_kurs']."'>ZAPLAC</a>";
       		 			
       		 			echo "</td>
       		        		 				        				  </tr>";
       		 			$i++;
       		 		}
       		 		echo "</table>";
       		 			
       		 		
       		 		
       		 		if($daty['do'] < date("Y-m-d H:i:s")) echo '<div><input class="kat_button" value="Drukuj mój plan zajęć" onclick="alert(\'Drukuje plan zajęć\')" type="submit" /></div>  ';
       		 		
       		 		
       		 	}
       		 
       		 	break;
       		 
       		 case 'student_zapisy_lista':
       		 			if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='3') {
       		 				echo "<script>document.location = 'start'</script>";
       		 			} else {
       		 				echo '<div class="content_t"><h1>Dziekanat COPER - Przeglądanie kursów</h1></div>
       		 		       		 	        		    		<div class="content_c">
       		 		       		 	        		   		<div class="dane">';
       		 				$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
       		 				
       		 				$daty = $kurs->getDaty();
       		 				
       		 				if($daty['od']< date("Y-m-d H:i:s") AND $daty['do']> date("Y-m-d H:i:s")) {
       		 				
	       		 				$sdata = $kurs->getAllKursy();
	       		 				//print_r($sdata);
	       		 				
	       		 				
	       		 				echo "<table>
	       		 				                		<tr>
	       		 				        				    <th>Lp.</th>
	       		 				        				    <th>Kod kursu</th>
	       		 				        				    <th>Kod grupy</th>
	       		 				        				    <th>Nazwa kursu</th>
	       		 				        				    <th>Prowadzacy</th>
	       		 				        				    <th>ECTS</th>
	       		 				        				    <th>Forma zaliczenia</th>
	       		 				        				    <th>Miejsca</th>
	       		 				        				    <th>Płatny</th>
	       		 				        				    <th>Opcje</th>
	       		 				        				  </tr>";
	       		 				$i=1;
	       		 				foreach($sdata as $sd){
	       		 					echo "
	       		 				        				  <tr>
	       		 				        				    <td>".$i."</td>
	       		 				        				    <td>".$sd['kod_kursu']."</td>
	       		 				        				    <td>".$sd['kod_grupy']."</td>
	       		 				        				    <td>".$sd['nazwa']."</td>
	       		 				        				    <td>".$sd['name']." ".$sd['surname']."</td>
	       		 				        				    <td>".$sd['ects']."</td>";
	       		 					if($sd['forma_zaliczenia']=='1') $fz = 'Egzamin';
	       		 					else $fz = 'Zaliczenie';
	       		 					echo "<td>".$fz."</td>";
	       		 				
	       		 					echo "<td>".$kurs->returnSpaceInKurs($sd['id_kurs'])."</td>";
	       		 						
	       		 					if($sd['platny']=='1') $platny = 'TAK - '.$sd['ile_platny'].' PLN';
	       		 					else $platny = 'NIE';
	       		 					echo "<td>".$platny."</td>";
	       		 				
	       		 					echo"<td><a href='student_zapisy_dodaj?id=".$sd['id_kurs']."'>ZAPISZ</a> </td>
	       		 				        				  </tr>";
	       		 					$i++;
	       		 				}
	       		 				echo "</table>";
       		 				} else {
       		 					if($daty['od'] > date("Y-m-d H:i:s")) $dty = "Jeszcze nie pora na zapisy !";
       		 					elseif($daty['do'] < date("Y-m-d H:i:s")) $dty = "Już po zapisach !";
       		 					echo $dty." Czas trwania zapisów to: ".$daty['od']." - ".$daty['do'];
       		 				}
       		 			}
       		 			 
      		break;

      		case 'student_zapisy_dodaj':
      			if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='3') {
      				echo "<script>document.location = 'start'</script>";
      			} else {
      				$vars = new vars();
      				echo '<div class="content_t"><h1>Dziekanat COPER - Zapis na kurs</h1></div>
      		       		         	                		    		<div class="content_c">
      		       		         	                		   		<div class="dane">';
      				if($vars->get('id')) {
      					$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
      					
      					
      					//sprawdz czy sa wolne miejsca
      					
      					if($kurs->returnSpaceInKurs($vars->show('id'))!='0') {
      					
      					
      						$zapis = $kurs->zapiszNaKurs($vars->show('id'), $_SESSION['user_id']);
      					
      					
      						if($zapis['status']) echo $zapis['why'].' <a href="student_zapisy_moje">zobacz kursy</a>';
      						else echo $zapis['why'].' <a href="student_zapisy_lista">wróć</a>';
      					} else echo 'Brak wolnych miejsc ! <a href="student_zapisy_lista">wróć</a>';
      				} else echo 'brak danych';
      			}
      	break;
      	case 'student_zapisy_wypisz':
      		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='3') {
      			echo "<script>document.location = 'start'</script>";
      		} else {
      			$vars = new vars();
      			echo '<div class="content_t"><h1>Dziekanat COPER - Wypis z kursu</h1></div>
      	      		       		         	                		    		<div class="content_c">
      	      		       		         	                		   		<div class="dane">';
      			if($vars->get('id')) {
      	
      				$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
      				$zapis = $kurs->WypiszZKursu($vars->show('id'));
      				 
      				 
      				echo $zapis['why'].' <a href="student_zapisy_moje">wróć</a>';
      			} else echo 'brak danych';
      		}
      		break;
      	
      	case 'student_zaplac':
      			if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='3') {
      				echo "<script>document.location = 'start'</script>";
      			} else {
      				$vars = new vars();
      				echo '<div class="content_t"><h1>Dziekanat COPER - Zapłać za kurs</h1></div>
      		      	      		       		         	                		    		<div class="content_c">
      		      	      		       		         	                		   		<div class="dane">';
      				if($vars->get('id')) {
      					 
      			
      					$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
      					$zapis = $kurs->ZaplacZaKurs($vars->show('id'), $_SESSION['user_id']); // to jest id kursu
      					 
      					 
      					echo $zapis['why'].' <a href="student_zapisy_moje">wróć</a>';
      					
      					echo "<img src='images/zaplac.png' alt='oplaty' />";
      					
      				} else echo 'brak danych';
      			}
      	break;
      	case 'student_oplaty':
      		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='3') {
      			echo "<script>document.location = 'start'</script>";
      		} else {
      			echo '<div class="content_t"><h1>Dziekanat COPER - Moje oplaty</h1></div>
      	       		 	                        		    		<div class="content_c">
      	       		 	                        		   		<div class="dane">';
      			$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
      			$sdata = $kurs->getOplaty($_SESSION['user_id']);
      			//print_r($sdata); die();
      			 
      			 
      			echo "<table>
      	       		 	                		<tr>
      	       		 	        				    <th>Lp.</th>
      	       		 	        				    <th>Przedmiot/Kod</th>
      	       		 	        				    <th>Kwota</th>
      	       		 	        				    <th>Kto działał</th>
      	       		 	        				   <th>Status</th>
      	       		 	        				  </tr>";
      			$i=1;
      			foreach($sdata as $sd){
      				echo "
      	       		 	        				  <tr>
      	       		 	        				    <td>".$i."</td>
      	       		 	        				    <td>".$sd['nazwa']." / ".$sd['kod_kursu']."</td>
      	       		 	        				    <td>".$sd['ile_platny']." PLN</td>
      								<td>".$sd['name']." ".$sd['surname']."</td>
      	       		 	        				    ";
      	
      				if($sd['id_status_oplat']=='1') $stat = 'Zatwierdzony';
      				elseif($sd['id_status_oplat']=='2') $stat = 'Odrzucony';
      				else $stat = 'Nowy';
      				echo "<td>".$stat."</td>";
      				"</tr>";
      				$i++;
      			}
      			echo "</table>";
      		}
      		 
      	break;
       	
      	
      	
      	/////////////////////////////////////////////
      	
      	case 'wykladowca':
      		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='2') {
      			echo "<script>document.location = 'start'</script>";
      		} else {
      			echo '<div class="content_t"><h1>Dziekanat COPER wita Wykładowce, co chcesz dziś zrobić ?</h1></div>
      	       		 	        		    		<div class="content_c">
      	       		 	        		   		<div class="dane">';
      			$user_data = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
      			$user = $user_data->getUserData($_SESSION['user_id']);
       		 			echo "<h2>WITAJ ".$user['name']." ".$user['surname']."</h2>";
       		 			
       		 			
       		 			$com = new comments(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
       		 			
       		 			$data = $com->getComments();
     
  
       		 			echo "<h3>Aktualności </h3>";
       		 			
       		 			foreach($data as $d) {
       		 				echo "<p><u>".$d['created_at']."</u> <strong>".$d['name']." ".$d['surname']."</strong> - ".$d['description']."</p>";
       		 			}
      			 
      		}
      		 
      		break;
      	
      	case 'wykladowca_lista_zajec':
      		if(!isset($_SESSION['user_id'])OR$_SESSION['user_type']!='2') {
      			echo "<script>document.location = 'start'</script>";
      		} else {
      			echo '<div class="content_t"><h1>Dziekanat COPER - Lista Twoich przedmiotów</h1></div>
      	       		 		       		 	        		    		<div class="content_c">
      	       		 		       		 	        		   		<div class="dane">';
      			$kurs = new kurs(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
      	
      			$daty = $kurs->getDaty();
      	
      			if($daty['do']< date("Y-m-d H:i:s")) { // skonczyly sie zapisy
      	
      				$sdata = $kurs->getAllKursyWykladowcy();
      				//print_r($sdata);
      				//die(); 
      				 
      				echo "<table>
      		       		 				                		<tr>
      		       		 				        				    <th>Lp.</th>
      		       		 				        				    <th>Kod kursu</th>
      		       		 				        				    <th>Kod grupy</th>
      		       		 				        				    <th>Nazwa kursu</th>
      		       		 				        				    <th>Miejsca ogółem</th>
      		       		 				        				    <th>Miejsca wymagane minimum</th>
      		       		 				        				    <th>Miejsca zajęte</th>
      		       		 				        				    <th>Status</th>
      		       		 				        				  </tr>";
      				$i=1;
      				foreach($sdata as $sd){
      					echo "
      		       		 				        				  <tr>
      		       		 				        				    <td>".$i."</td>
      		       		 				        				    <td>".$sd['kod_kursu']."</td>
      		       		 				        				    <td>".$sd['kod_grupy']."</td>
      		       		 				        				    <td>".$sd['nazwa']."</td>
      		       		 				        				    <td>".$sd['miejsca']."</td>
      		       		 				        				    <td>".$sd['miejsca_minimum']."</td>";
					$zajete_miejsca = $kurs->returnBizzySpaceInKurs($sd['id_kurs']);
      					echo "<td>".$zajete_miejsca."</td>";
      					
      					if($zajete_miejsca>=$sd['miejsca_minimum']) $stat = 'Uruchomiony';
      					else $stat = 'Nie uruchomiony - brak minimalnej ilości studentów';
      					echo "<td>".$stat."</td>";
      					 
      					echo"</tr>";
      					$i++;
      				}
      				echo "</table>";
      				
      				
      				echo '<div><input class="kat_button" value="Drukuj mój plan zajęć" onclick="alert(\'Drukuje plan zajęć\')" type="submit" /></div>  ';
      				echo '<div><input class="kat_button" value="Drukuj karte pracy" onclick="alert(\'Drukuje karte pracy\')" type="submit" /></div>  ';
      				
      			} else {
      				echo "Zapisy sie jeszcze nie skończyły !";
      			}
      		}
      	
      		break;
      	
		default:
			echo '<div class="content_t"><h1>Błąd 404</h1></div>
    		<div class="content_c">
   		<div class="dane">';
			echo "Nie ma takiej strony";
			//echo "<script>document.location = 'wejscie.html'</script>";
		break;
	  }

?>
    
    	</div>
   
    </div>
    <div class="content_b"></div>
</div>
<!-- koniec content -->

<br /><br /><br />
<br /><br /><br />

<div class="footer">
	<div class="footer_dane">
		<div class="footer_left"><p></p></div>
    	<div class="footer_right">&copy; COPER 2011</p></div>
    	<div class="footer_clear"></div>
   	</div>
</div>
</body>
</html>
